package com.newprojectsql.ambulante.activity.Vente;

import com.newprojectsql.ambulante.activity.GestionClients.Adresse;

import java.io.Serializable;
import java.util.Date;

public class Facture implements Serializable {

    public int id_facture;
    public String num_facture;
    public String adresse_liv;
    public String adresse_fac;
    public Double credit;
    public Date date;
    public int id_client;
    public Double prix_ht;
    public Double prix_ttc;
    public Double prix_ttc_total;
    public String raison_sociale;
    public Double remise;
    public Double timbre_f;
    public Double total_reglement;
    public boolean is_paid;



    public Facture(String num_facture, int id_client, String raison_sociale, Double prix_ht, Double prix_ttc) {

        this.num_facture = num_facture;
        this.id_client = id_client;
        this.raison_sociale = raison_sociale;
        this.prix_ht = prix_ht;
        this.prix_ttc = prix_ttc;

    }


    public Facture (String num_facture, int id_client, Double prix_ht, Double remise, Double timbre_f, Double prix_ttc_total)
    {
        this.id_client= id_client;
        this.num_facture = num_facture;
        this.prix_ht = prix_ht;
        this.remise = remise;
        this.timbre_f = timbre_f;
        this.prix_ttc_total = prix_ttc_total;

    }



    public Facture() {

    }


    public void setPrix_ttc_total(Double prix_ttc_total) {
        this.prix_ttc_total = prix_ttc_total;
    }

    public void setAdresse_liv(String adresse_liv) {
        this.adresse_liv = adresse_liv;
    }

    public void setAdresse_fac(String adresse_fac) {
        this.adresse_fac = adresse_fac;
    }

    public String getAdresse_liv() {
        return adresse_liv;
    }

    public String getAdresse_fac() {
        return adresse_fac;
    }

    public Double getPrix_ttc_total() {
        return prix_ttc_total;
    }

    public void setId_facture(int id_facture) {
        this.id_facture = id_facture;
    }

    public void setNum_facture(String num_facture) {
        this.num_facture = num_facture;
    }


    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }



    public void setPrix_ht(Double prix_ht) {
        this.prix_ht = prix_ht;
    }

    public void setPrix_ttc(Double prix_ttc) {
        this.prix_ttc = prix_ttc;
    }

    public void setRaison_sociale(String raison_sociale) {
        this.raison_sociale = raison_sociale;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public void setTimbre_f(Double timbre_f) {
        this.timbre_f = timbre_f;
    }

    public void setTotal_reglement(Double total_reglement) {
        this.total_reglement = total_reglement;
    }


    public void setIs_paid(boolean is_paid) {
        this.is_paid = is_paid;
    }


    public boolean isIs_paid() {
        return is_paid;
    }

    public int getId_facture() {
        return id_facture;
    }

    public String getNum_facture() {
        return num_facture;
    }


    public Double getCredit() {
        return credit;
    }

    public Date getDate() {
        return date;
    }

    public int getId_client() {
        return id_client;
    }


    public Double getPrix_ht() {
        return prix_ht;
    }

    public Double getPrix_ttc() {
        return prix_ttc;
    }

    public String getRaison_sociale() {
        return raison_sociale;
    }

    public Double getRemise() {
        return remise;
    }

    public Double getTimbre_f() {
        return timbre_f;
    }

    public Double getTotal_reglement() {
        return total_reglement;
    }



    @Override
    public String toString() {

        return num_facture;

    }




}
