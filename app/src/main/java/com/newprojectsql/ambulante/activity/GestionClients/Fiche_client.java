package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Reglement.Activity_RegPrincipale;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Address;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fiche_client extends AppCompatActivity {

    private static final String TAG = Fiche_client.class.getSimpleName();

    private TextView txt_raison;
    private TextView txt_code;
    private TextView txt_matricule;
    private TextView txt_mobile;
    private TextView txt_fixe;
    private TextView txt_email;
    private TextView txt_registre;

    private Button btn_vente;
    private Button btn_reglement;


    Api mService;

    public static Client currentClient;

 //   public Client cppp;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fiche_client);
        Log.d(TAG, "OnCreate:started.");

        mService=RetrofitClient.getInstance().getApi();


       ////////////  list des clients//////////////////////
        currentClient = (Client) getIntent().getSerializableExtra("CLIENT");

        txt_code = (TextView) findViewById(R.id.txt_codeclient);
        txt_raison = (TextView) findViewById(R.id.txt_raison);
        txt_matricule = (TextView) findViewById(R.id.txt_matricule);
        txt_mobile = (TextView) findViewById(R.id.txt_tel_mobile);
        txt_fixe = (TextView) findViewById(R.id.txt_tel_fixe);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_registre = (TextView) findViewById(R.id.txt_registre);

        btn_vente = (Button) findViewById(R.id.btn_vente);
        btn_reglement = (Button) findViewById(R.id.btn_reglement);

        txt_code.setText(currentClient.code_client);
        txt_raison.setText(currentClient.raison_sociale);
        txt_matricule.setText(currentClient.matricule_fiscale);
        txt_mobile.setText(currentClient.tel_mobile);
        txt_fixe.setText(currentClient.tel_fixe);

        txt_email.setText(currentClient.email);
        txt_registre.setText(currentClient.registre_commerce);


        getListAdr(currentClient.id_client);



        btn_vente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              try {

                 //To pass:
                      // in Activity A, when starting Activity B
               Intent intent = new Intent(Fiche_client.this, Vente.class);
               intent.putExtra("CLIENT", currentClient);
               Fiche_client.this.startActivity(intent);

               }

              catch (Exception Ex)
               {
                 Toast.makeText(Fiche_client.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });




        btn_reglement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    //To pass:
                    // in Activity A, when starting Activity B
                    Intent intent = new Intent(Fiche_client.this, Activity_RegPrincipale.class);
                    intent.putExtra("CLIENT", currentClient);
                    Fiche_client.this.startActivity(intent);

                }

                catch (Exception Ex)
                {
                    Toast.makeText(Fiche_client.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }








    private void getListAdr(int id) {
        mService.getListAdr(id).enqueue(new Callback<List<Adresse>>() {
            @Override
            public void onResponse(Call<List<Adresse>> call, Response<List<Adresse>> response) {

           //     Toast.makeText(Fiche_client.this, ""+response.body().toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<List<Adresse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }









}
