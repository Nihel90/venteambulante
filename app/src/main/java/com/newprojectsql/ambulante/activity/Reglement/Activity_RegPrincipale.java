package com.newprojectsql.ambulante.activity.Reglement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.Client;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Vente.Facture;
import com.newprojectsql.ambulante.activity.Vente.LigneFacture;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_RegPrincipale extends Activity implements AdapterView.OnItemSelectedListener{


    /////Declaration Api
    Api mService;

    ///Declaration des Views

    private TextView txt_solde_client;

    private TextView txt_num_facture;
    private TextView txt_net_payer;
    private Spinner spinner_reg, spinner_client, spinner_facture;
    private EditText montant;
    private Button btn_ajout_reg;
    private ListView list;
    private TextView txt_total_reg;
    private TextView txt_reste_payer;
    private Button btn_valider;

    private TextView txt_num_reglement;
    private TextView txt_type_reglement;
    private TextView txt_montant;


    //////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    static final String KEY_NOM_PRODUIT = "nomP";
    static final String KEY_CODE_PRODUIT = "codeP";
    static final String KEY_PRIX_U = "prixU";
    static final String KEY_QUANTITE = "quantite";
    static final String KEY_PRIX_TOTAL = "prixTotal";
    static final String KEY_PRIX_TTC = "prix_ttc";
    static final String KEY_PRIX_TTC_TOTAL = "prix_ttc_total";
    static final String KEY_TVA = "tva";
    static final String KEY_NUM_REGLEMENT = "num_reglement";
    static final String KEY_TYPE_REGLEMENT = "type_reglement";
    static final String KEY_MONTANT = "montant";

    ///////////////////////////////////////////////////
    /////////////////////////////////////////////////


    //Declaration des listes

    private List<Client> listeClients;
    private List<Facture> listeFactures;


    ///Declaration des variables

    public Double totalReg=0.0;
    public Double reste=0.0;
    public double tva=0.0;
    public Double totalC=0.0;


    public Facture currentfacture;
    public Reglement currentReglement;
    public  Client currentClient=null;


    AdapterListReglement adapter;
    ArrayList<HashMap<String, String>> reglementList;

    public int deleteItem;
    public String mnt;
    public double montantReg=0.0;
    public int id_client;
    public int id_facture;

    public String raison_sociale;
    public String num_facture;
    public Double net_a_payer;

    public double solde_client=0.0;
    public double max_credit = 0.0;

    public   int testExist=-1;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_principale);


        mService = RetrofitClient.getInstance().getApi();

        txt_num_facture = (TextView) findViewById(R.id.txt_num_facture);
        txt_solde_client =(TextView) findViewById(R.id.txt_solde_client);
        txt_net_payer = (TextView) findViewById(R.id.txt_net_payer);


        ///////////////les spinners*//////////////////////////
        spinner_reg = (Spinner) findViewById(R.id.spinnerR);
        spinner_client = (Spinner) findViewById(R.id.spinner_client);
        spinner_facture = (Spinner) findViewById(R.id.spinner_facture);
        montant = (EditText) findViewById(R.id.montant);


        ///////////////////////////les boutons/////////////////////
        btn_ajout_reg = (Button) findViewById(R.id.btn_ajout_reg);
        btn_valider = (Button)findViewById(R.id.btn_valider);


        list= (ListView)findViewById(R.id.list);
        txt_total_reg = (TextView) findViewById(R.id.txt_total_reg) ;
        txt_reste_payer = (TextView) findViewById(R.id.txt_reste_payer);


        txt_num_reglement = (TextView) findViewById(R.id.txt_num_reglement);
        txt_type_reglement = (TextView) findViewById(R.id.txt_type_reglement);
        txt_montant = (TextView) findViewById(R.id.txt_montant);



        currentReglement = new Reglement();
        currentfacture = new Facture();


        listeClients = new ArrayList<>();
        listeFactures = new ArrayList<>();

        currentClient = (Client) getIntent().getSerializableExtra("CLIENT");





        if (currentClient==null){

            listeClients = getListeClt();


            // Spinner click listener

            spinner_client.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    // On selecting a spinner item

                    currentClient = listeClients.get(position);
                    id_client = currentClient.id_client;
                    raison_sociale = currentClient.raison_sociale;

                    // On selecting a spinner item

                    listeFactures = getFacturesNonR(id_client);


                    ////////////////////get solde client= total credit///////////////

                    Call<Double> call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .getTotalCredit(id_client);

                    call.enqueue(new Callback<Double>() {
                        @Override
                        public void onResponse(Call<Double> call, Response<Double> response) {

                            Double s;

                            s=response.body();

                            txt_solde_client.setText(s.toString());
                            solde_client= s;
                        //    Toast.makeText(Activity_RegPrincipale.this, ""+s, Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Call<Double> call, Throwable t) {


                        }
                    });
                    /////////////////////////////////////////////////////////////////////////////////////////

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });



                spinner_facture.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        // On selecting a spinner item
                        currentfacture = listeFactures.get(position);


                        num_facture = currentfacture.num_facture;
                        txt_num_facture.setText(num_facture);

                        net_a_payer = currentfacture.credit;
                        txt_net_payer.setText(net_a_payer.toString());
                        txt_reste_payer.setText(net_a_payer.toString());

                        id_facture = currentfacture.id_facture;

                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });



        }

        else {

            try {


                String[] items = new String[] {currentClient.raison_sociale};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_client.setAdapter(adapter);

                spinner_client.setEnabled(false);

                id_client = currentClient.id_client;
                raison_sociale = currentClient.raison_sociale;


                // On selecting a spinner item

                listeFactures = getFacturesNonR(id_client);


 ///////////////////////////get solde client = total credit/////////////////////////

                Call<Double> call2 = RetrofitClient
                        .getInstance()
                        .getApi()
                        .getTotalCredit(id_client);

                call2.enqueue(new Callback<Double>() {
                    @Override
                    public void onResponse(Call<Double> call, Response<Double> response) {


                        Double s;

                        s=response.body();
                        txt_solde_client.setText(s.toString());

                        solde_client=s;

                    }

                    @Override
                    public void onFailure(Call<Double> call, Throwable t) {


                    }
                });

      ////////////////////////////////////////////////////////////////////////////////////


                spinner_facture.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



                            // On selecting a spinner item
                            currentfacture = listeFactures.get(position);
                            num_facture = currentfacture.num_facture;
                            txt_num_facture.setText(num_facture);
                            net_a_payer = currentfacture.credit;
                            txt_net_payer.setText(net_a_payer.toString());
                            txt_reste_payer.setText(net_a_payer.toString());
                            id_facture = currentfacture.id_facture;
                       //     Toast.makeText(Activity_RegPrincipale.this, "" + id_facture, Toast.LENGTH_SHORT).show();

                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            catch (Exception ex)
            {
                Toast.makeText(this, ex.getMessage() , Toast.LENGTH_SHORT).show();}

        }









        //////////////Détails Facture /////////////


        txt_total_reg.setText(totalReg.toString());

        txt_reste_payer.setText(reste.toString());



        /////Initialiser Spinner Type de règlement:


        // Spinner Drop down elements
        final List<String> categories = new ArrayList<String>();
        //  categories.add("");
        categories.add("Espèce");
        categories.add("Chèque");
        categories.add("Traite");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter <String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_reg.setAdapter(dataAdapter);


        spinner_reg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item

                String pos= categories.get(position);
                currentReglement.type_reglement= pos;
              //  Toast.makeText(Activity_RegPrincipale.this, ""+currentReglement.type_reglement, Toast.LENGTH_SHORT).show();

                // On selecting a spinner item

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        //////////////////////Code for list view////////////////////////////////
        ///////////////////////////////////////////////////////////////////////
        reglementList = new ArrayList<HashMap<String, String>>();
        adapter = new AdapterListReglement(Activity_RegPrincipale.this, reglementList);



        ///////////onClick de button ajout_reglement////////////
        btn_ajout_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Insert_Data_inList();
                Clear_Montant();

            }
        });




        //////////onClick de button valider////////////////////
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reglementList.size()!=0) {
                    new AlertDialog.Builder(Activity_RegPrincipale.this)
                            .setTitle("Cloture Vente")
                            .setMessage("Etes vous sûr de vouloir valider ce(s) règlement(s) ?")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {

                                        UpdateVals();
                                        AddReglement();
                                        Update_Facture();

                                        if (currentfacture.credit == 0) {
                                            Update_Paid_Facture();
                                        }

                                        Intent activity = new Intent(Activity_RegPrincipale.this, MainActivity.class);
                                        startActivity(activity);

                                    } catch (Exception Ex) {
                                        Toast.makeText(Activity_RegPrincipale.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                                    }


                                }
                            }).setNegativeButton("Non", null).show();

                }

                else
                {
                    Toast.makeText(Activity_RegPrincipale.this, "Veuillez saisir un montant", Toast.LENGTH_SHORT).show();
                }

            }

        });


    }



    private List<Client> getListeClt() {

        mService.getListeClt().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {


                listeClients = response.body();
                ArrayAdapter<Client> dataAdapterClients = new ArrayAdapter<Client>(Activity_RegPrincipale.this, android.R.layout.simple_spinner_item, listeClients);
                dataAdapterClients.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_client.setAdapter(dataAdapterClients);


            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {

            }
        });

        return listeClients;
    }


    private List<Facture> getFacturesNonR(int i) {

        mService.getFacturesNonR(i).enqueue(new Callback<List<Facture>>() {
            @Override
            public void onResponse(Call<List<Facture>> call, Response<List<Facture>> response) {


                listeFactures=response.body();
                ArrayAdapter<Facture> dataAdapterFacture = new ArrayAdapter<Facture>(Activity_RegPrincipale.this, android.R.layout.simple_spinner_item, listeFactures);
                dataAdapterFacture.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_facture.setAdapter(dataAdapterFacture);

                if (listeFactures.size()==0)
                {
                    txt_num_facture.setText("");
                    txt_net_payer.setText("");
                }


            }

            @Override
            public void onFailure(Call<List<Facture>> call, Throwable t) {

            }
        });

        return listeFactures;
    }





    public void Update_Paid_Facture()
    {
        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .UpdatePaidFacture(currentfacture.id_facture);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String s = null;

                try {

                    s = response.body().string();
                    String h = s.replace("\"", "");

                    Toast.makeText(Activity_RegPrincipale.this, s, Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(Activity_RegPrincipale.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }



    public void Update_Facture()
    {

        Double tot = Double.valueOf(txt_total_reg.getText().toString().trim());
        Double res = Double.valueOf(txt_reste_payer.getText().toString().trim());
        currentfacture.total_reglement= currentfacture.total_reglement + tot;
        currentfacture.credit = res;

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .updateFacture(currentfacture.total_reglement, currentfacture.credit, currentfacture.id_facture);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String s = null;

                try {

                    s = response.body().string();
                    String h = s.replace("\"", "");

                    Toast.makeText(Activity_RegPrincipale.this, s, Toast.LENGTH_SHORT).show();

                    Update_Client();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(Activity_RegPrincipale.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


    }




    public void Clear_Montant()
    {
        montant.setText("");
    }





    public void  AddReglement() {

        for (int i = 0; i < reglementList.size(); i++) {
            currentReglement.type_reglement = reglementList.get(i).get(KEY_TYPE_REGLEMENT);
            currentReglement.montant = Double.valueOf(reglementList.get(i).get(KEY_MONTANT));


            /*Do add Lignefacture using the api call*/
            Call<ResponseBody> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .createReglement(  currentReglement.type_reglement, currentReglement.montant, currentfacture.id_facture);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    String s = null;

                    try {

                        s = response.body().string();
                        Toast.makeText(Activity_RegPrincipale.this, "" + s, Toast.LENGTH_SHORT).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        }
    }









    public void Insert_Data_inList() {

        mnt = montant.getText().toString().trim();


        //////////////déclaration es variables pour tester l'existance du règlement

        boolean test = false;


        for (int i=0; i<reglementList.size(); i++) {

            if (reglementList.get(i).get(KEY_TYPE_REGLEMENT).equals(currentReglement.type_reglement))
            {
                testExist =i;
                test= true;                        }

        }




        try {

            if (mnt.length() > 0) {
                montantReg = Double.valueOf(mnt);
                montantReg= Math.round(montantReg*1000.0)/1000.0;

                if ((montantReg > 0)&& (montantReg<= Double.valueOf(txt_reste_payer.getText().toString().trim()))){

                    if (!test) {

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(KEY_NUM_REGLEMENT, reglementList.size() + 1 + "");
                        map.put(KEY_TYPE_REGLEMENT, currentReglement.type_reglement);
                        map.put(KEY_MONTANT, montantReg + "");

                        reglementList.add(map);
                        list.setAdapter(adapter);

                        adapter.notifyDataSetChanged();

                        spinner_client.setEnabled(false);



                        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                            @Override
                            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                           int pos, long arg3) {
                                deleteItem = pos;
                                // Creating a new alert dialog to confirm the delete
                                AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                                        .setTitle("Etes vous sûr de vouloir supprimer ce règlement")
                                        .setPositiveButton("Oui",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {

                                                        try {
                                                            reglementList.remove(deleteItem);

                                                            adapter.notifyDataSetChanged();
                                                            dialog.dismiss();
                                                            UpdateVals();

                                                        } catch (Exception e) {
                                                            Toast.makeText(Activity_RegPrincipale.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                        }

                                                    }
                                                })
                                        .setNegativeButton("Non",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {
                                                        // When you press cancel, just close the
                                                        // dialog
                                                        dialog.cancel();
                                                    }
                                                }).show();

                                return false;

                            }
                        });

                        UpdateVals();
                    }

                    else
                    {

                             new AlertDialog.Builder(Activity_RegPrincipale.this)
                                .setTitle("Valider le montant saisi pour ce type de règlement ")
                                .setMessage("Etes vous sûr de vouloir mettre à jour ce montant ?")
                                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        double q = Double.valueOf(reglementList.get(testExist).get(KEY_MONTANT));
                                        double sommeMontant= q + montantReg;

                                        reglementList.get(testExist).put(KEY_MONTANT, sommeMontant + "");
                                        UpdateVals();

                                        list.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                    }})


                                .setNegativeButton("Non", null).show();

                    }


                } else {

                    Toast.makeText(this, "Le reste à payer est " +Double.valueOf(txt_reste_payer.getText().toString().trim()), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Veuillez insérer le montant", Toast.LENGTH_SHORT).show();
            }

        }
        catch (Exception Ex) {
            Toast.makeText(this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }




    public void UpdateVals()
    {


    Double tot= 0.0;

    Double res=currentfacture.credit;

    for (int i=0; i<reglementList.size(); i++)
    {

        montantReg=Double.valueOf(reglementList.get(i).get(KEY_MONTANT));
        tot=tot+montantReg;

    }

        res = res-tot;
        res= Math.round(res*1000.0)/1000.0;

        tot= Math.round(tot*1000.0)/1000.0;

        txt_total_reg.setText(tot.toString());
        txt_reste_payer.setText(res.toString());


    }




    public void Update_Client()
    {

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .updateClient(currentfacture.id_client);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                String s = null;

                try {

                    s = response.body().string();
                    // String h = s.replace("\"", "");

                    Toast.makeText(Activity_RegPrincipale.this, "" + s, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });





    }





    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }








    }
