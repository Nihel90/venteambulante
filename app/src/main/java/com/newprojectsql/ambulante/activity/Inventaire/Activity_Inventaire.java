package com.newprojectsql.ambulante.activity.Inventaire;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Vente.Categorie;
import com.newprojectsql.ambulante.activity.Vente.Produit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.SHARED_PREFS;
import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.TEXT;

public class Activity_Inventaire extends AppCompatActivity {


    /////Declaration Api
    Api mService;
    Context context;


    /////Declaration des listes

    private List<Categorie> listeCategories;
    private List<Produit> listeProduits;
    private List<Produit> listeCodes;



    private Spinner  spinnerCategorie, spinnerNomP;
    private Button btn_date, btn_valider, btn_valider_inv;

    private EditText qte;
    private EditText eText;


    public  Categorie currentCategorie = null;
    public Produit currentProduit = null;
    int idCategorie, idProduit;

    int idDepot;
    String date;
    public String qteInv;
    public int qteP;
    public int nombre_inventaire;
    public String num_inventaire;

    public int q ;
    public int sumQte= 0;


    // contains the id of the item we are about to delete
    public int deleteItem;
    public   int testExist=-1;


    ////////////////code pour list 1//////////////////
//////////////////////////////////////////////
///////////////////////////////////////////////////////

    static final String KEY_QUANTITE_RES = "quantite_restante";
    static final String KEY_QUANTITE_INV = "quantite_inventaire";
    static final String KEY_NOM_PRODUIT = "nom_produit";
    static final String KEY_CODE_PRODUIT = "code_produit";
    static final String KEY_NOM_CATEGORIE = "nom_categorie";
    static final String KEY_SUM_Quantite = "sum_quantite";
    static final String KEY_QUANTITE_VENDUE = "quantite_vendue";
    static final String KEY_ECART = "ecart";


    private ListView list;

    private Produit produit;
    private TextView code_prod;
    private TextView nom_prod;
    private TextView quantite;
    private TextView qteChargee;
    private TextView qteVendue;

    String c= "";

    AdapterListProd1_Inv adapter;
    ArrayList<HashMap<String, String>> produitList;


    ///////////////////////code pour liste 2///////////////////
    //////////////////////////////////////////////////

    private ListView list2;


    private TextView categorie;
    private TextView quantite_inv;
    private TextView quantite_phys;
    private TextView nom_produit;
    private TextView ecart;


    AdapterListProd2_Inv adapter2;
    ArrayList<HashMap<String, String>> produitList2;
   //////////////////////////////////////////////////
    ///////////////////////////////////////////////



    /////////////////////DateListener Picker///////////
    //////////////////////////////////////////
    DatePickerDialog datePickerDialog;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventaire);

        mService = RetrofitClient.getInstance().getApi();



        //////////////////SharedPreferences  get idDep : donnée sauvegardée dans "Paramètres"////////////////////////

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String p = sharedPreferences.getString(TEXT, "");

        idDepot = Integer.valueOf(p);
        Toast.makeText(this, ""+idDepot, Toast.LENGTH_SHORT).show();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////



        ////////////////////////get nbombre des inventaires////////////////////////
        Call<Integer> call2 = RetrofitClient
                .getInstance()
                .getApi()
                .getNombreInventaire();


        call2.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);

                int s= response.body();

                nombre_inventaire = s;
                nombre_inventaire = nombre_inventaire + 1;
                num_inventaire = "Inventaire " + year + "_" + nombre_inventaire;

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });







        ///déclarer les listes
        listeCategories = new ArrayList<>();
        listeProduits = new ArrayList<>();
        listeCodes = new ArrayList<>();


        // Spinner element
        spinnerCategorie = findViewById(R.id.spinnerCategorie);
        spinnerNomP = findViewById(R.id.spinnerNomP);


        ///editText, TextView et buttons
        qte = findViewById(R.id.quantite);
        btn_valider = findViewById(R.id.btn_valider);
        btn_date = findViewById(R.id.btn_date);
        btn_valider_inv = findViewById(R.id.btn_valider_inv);
        eText = findViewById(R.id.eText);


        /////////////////////////list 1////////////////////////////
        //////////////////////////////////////////////////////
        produitList = new ArrayList<HashMap<String, String>>();
        adapter = new AdapterListProd1_Inv(Activity_Inventaire.this, produitList);

        code_prod = findViewById(R.id.code_produit);
        nom_prod = findViewById(R.id.nom_produit);
        quantite = findViewById(R.id.qte_restante);
        qteChargee = findViewById(R.id.qte_chargee);
        qteVendue = findViewById(R.id.qte_vendue);


        produit = new Produit();
        list = findViewById(R.id.list1);


        //////////////////////liste 2/////////////////////////
        ///////////////////////////////////////////////////////

        produitList2 = new ArrayList<HashMap<String, String>>();
        adapter2 = new AdapterListProd2_Inv(Activity_Inventaire.this, produitList2);


        categorie = findViewById(R.id.categorie);
        quantite_inv = findViewById(R.id.qte_inv);
        quantite_phys = findViewById(R.id.qte_physique);
        nom_produit = findViewById(R.id.nom_produit);
        ecart = findViewById(R.id.ecart);

        list2 = findViewById(R.id.list2);


        //////////get CurrentDate /////////////////////////////

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCurrentDate();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String s = null;

                try {
                    s = response.body().string();
                    final String h = s.replace("\"", "");


                    eText.setText(h);


                    /////////////////get list of codes on date //////////////////

                    Call<List<Produit>> call2 = RetrofitClient
                            .getInstance()
                            .getApi()
                            .getCodeOnDate(h, idDepot);

                    call2.enqueue(new Callback<List<Produit>>() {
                        @Override
                        public void onResponse(Call<List<Produit>> call, Response<List<Produit>> response) {

                            listeCodes = response.body();

                            for (int i = 0; i < listeCodes.size(); i++) {

                                ///////////////// total quantite chargee de chaque produit///////////////////

                                Call<Integer> call4 = RetrofitClient
                                        .getInstance()
                                        .getApi()
                                        .getSumQuantiteOnDate(listeCodes.get(i).code_produit, h, idDepot);

                                final int finalI = i;
                                call4.enqueue(new Callback<Integer>() {
                                    @Override
                                    public void onResponse(Call<Integer> call, Response<Integer> response) {

                                        int s = response.body();

                                        final int sumQte = s;


                                        ///////////////////get Qte vendue //////////////

                                        try {

                                            Call<Integer> call10 = RetrofitClient
                                                    .getInstance()
                                                    .getApi()
                                                    .getQteVendueOnDate(listeCodes.get(finalI).code_produit, h);

                                            call10.enqueue(new Callback<Integer>() {
                                                @Override
                                                public void onResponse(Call<Integer> call, Response<Integer> response) {

                                                    int t = response.body();
                                                    int qteVendue = t;


                                                    ////////calculer qteRestante///////////

                                                    int qteRest = sumQte - qteVendue;

                                                    InsertDataInList(listeCodes.get(finalI).code_produit, listeCodes.get(finalI).nom_produit, sumQte, qteVendue, qteRest);
                                                }

                                                @Override
                                                public void onFailure(Call<Integer> call, Throwable t) {

                                                }
                                            });
                                        } catch (Exception ex) {
                                            Toast.makeText(Activity_Inventaire.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t) {

                                    }
                                });


                            }

                        }

                        @Override
                        public void onFailure(Call<List<Produit>> call, Throwable t) {

                        }
                    });


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


        ////charger spinnerCategorie avec la liste des actegories
        listeCategories = getListCategories();


        ////////////////spinner categorie/////////////////////////
        spinnerCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentCategorie = listeCategories.get(position);
                idCategorie = currentCategorie.id_categorie;
                listeProduits = getListProduit(idCategorie);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ////////////////spinner produits//////////////
        spinnerNomP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentProduit = listeProduits.get(position);
                idProduit = currentProduit.id_produit;

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LancerInventaire();
                Clear_Quantite();
            }
        });



        btn_valider_inv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (produitList2.size()!=0) {


                    Call<Boolean> call3 = RetrofitClient
                            .getInstance()
                            .getApi()
                            .isInventaireExist(idDepot, eText.getText().toString().trim());

                    call3.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                                Boolean s = response.body();

                               if(s)
                                {

                                    new AlertDialog.Builder(Activity_Inventaire.this)
                                            .setTitle("Un inventaire existe déjà ")
                                            .setMessage("Souhaitez-vous mettre à jour cet inventaire?")
                                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                    for (int i= 0; i<produitList2.size(); i++) {

                                        final String code_prod = produitList2.get(i).get(KEY_CODE_PRODUIT);
                                        final int qteInv = Integer.parseInt(produitList2.get(i).get(KEY_QUANTITE_INV));
                                        int ecart = Integer.parseInt(produitList2.get(i).get(KEY_ECART));
                                        String date = eText.getText().toString().trim();
                                        int qteChargee = Integer.parseInt(produitList.get(i).get(KEY_SUM_Quantite));
                                        int qteVendue = Integer.parseInt(produitList.get(i).get(KEY_QUANTITE_VENDUE));
                                        int qteRestante = Integer.parseInt(produitList.get(i).get(KEY_QUANTITE_RES));
                                        String nomProduit = produitList.get(i).get(KEY_NOM_PRODUIT);


                                        Call<ResponseBody> call2 = RetrofitClient
                                                .getInstance()
                                                .getApi()
                                                .updateLigneInventaire(qteInv, ecart, code_prod, date, idDepot, qteChargee, qteVendue, qteRestante, nomProduit);

                                        call2.enqueue(new Callback<ResponseBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                                                try {
                                                    String s = response.body().string();
                                                    Toast.makeText(Activity_Inventaire.this, ""+s, Toast.LENGTH_SHORT).show();

                                                    Call<ResponseBody> call5 = RetrofitClient
                                                            .getInstance()
                                                            .getApi()
                                                            .updateStock(qteInv, code_prod, idDepot);

                                                    call5.enqueue(new Callback<ResponseBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                                            try {
                                                                String s = response.body().string();
                                                                Toast.makeText(Activity_Inventaire.this, ""+s, Toast.LENGTH_SHORT).show();

                                                                Intent intent = new Intent(Activity_Inventaire.this, MainActivity.class);
                                                                startActivity(intent);
                                                            }
                                                            catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                        }
                                                    });

                                                }
                                                catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseBody> call, Throwable t) {

                                            }
                                        });

                                       }

                                  }
                             })
                            .setNegativeButton("Non", null).show();

                                }

                               else
                              {

                                  new AlertDialog.Builder(Activity_Inventaire.this)
                                          .setTitle("Clôture Inventaire")
                                          .setMessage("Etes vous sûr de vouloir valider cet inventaire?")
                                          .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                              @Override
                                              public void onClick(DialogInterface dialog, int which) {



                                                  Call<ResponseBody> call = RetrofitClient
                                                          .getInstance()
                                                          .getApi()
                                                          .createInventaire(eText.getText().toString().trim(), num_inventaire, idDepot);

                                                  call.enqueue(new Callback<ResponseBody>() {
                                                      @Override
                                                      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                                                          try {
                                                              String s= response.body().string();
                                                              int id_inv = Integer.valueOf(s);


                                                              for (int i= 0; i<produitList2.size(); i++) {


                                                                  String nom_prod = produitList2.get(i).get(KEY_NOM_PRODUIT);
                                                                  final String code_prod = produitList2.get(i).get(KEY_CODE_PRODUIT);
                                                                  int qteRest = Integer.parseInt(produitList2.get(i).get(KEY_QUANTITE_RES));
                                                                  final int qteInv = Integer.parseInt(produitList2.get(i).get(KEY_QUANTITE_INV));
                                                                  int ecart = Integer.parseInt(produitList2.get(i).get(KEY_ECART));
                                                                  int qte_chargee = Integer.parseInt(produitList.get(i).get(KEY_SUM_Quantite));
                                                                  int qte_vendue = Integer.parseInt(produitList.get(i).get(KEY_QUANTITE_VENDUE));


                                                                  Call<ResponseBody> call3 = RetrofitClient
                                                                          .getInstance()
                                                                          .getApi()
                                                                          .createLigneInventaire(nom_prod, code_prod, qte_chargee, qte_vendue,qteRest, qteInv, ecart, id_inv);

                                                                  call3.enqueue(new Callback<ResponseBody>() {
                                                                      @Override
                                                                      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                                                          try {
                                                                              String s = response.body().string();
                                                                              Toast.makeText(Activity_Inventaire.this, ""+s, Toast.LENGTH_SHORT).show();


                                                                              Call<ResponseBody> call5 = RetrofitClient
                                                                                      .getInstance()
                                                                                      .getApi()
                                                                                      .updateStock(qteInv, code_prod, idDepot);

                                                                              call5.enqueue(new Callback<ResponseBody>() {
                                                                                  @Override
                                                                                  public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                                                                      try {
                                                                                          String s = response.body().string();
                                                                                          Toast.makeText(Activity_Inventaire.this, ""+s, Toast.LENGTH_SHORT).show();

                                                                                          Intent intent = new Intent(Activity_Inventaire.this, MainActivity.class);
                                                                                          startActivity(intent);
                                                                                      }
                                                                                      catch (IOException e) {
                                                                                          e.printStackTrace();
                                                                                      }
                                                                                  }

                                                                                  @Override
                                                                                  public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                                                  }
                                                                              });


                                                                          }
                                                                          catch (IOException e) {

                                                                              e.printStackTrace();
                                                                          }

                                                                      }

                                                                      @Override
                                                                      public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                                      }
                                                                  });

                                                              }

                                                          } catch (IOException e) {

                                                              e.printStackTrace();
                                                          }
                                                      }

                                                      @Override
                                                      public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                      }
                                                  });

                                              }
                                          }).setNegativeButton("Non", null).show();


                                  }

                              }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });

                }

                else
                {
                    Toast.makeText(Activity_Inventaire.this, "Veuillez insérer les produits", Toast.LENGTH_SHORT).show();

                }

            }
        });



        eText.setInputType(InputType.TYPE_NULL);
        eText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                datePickerDialog = new DatePickerDialog(Activity_Inventaire.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText.setText(year + "-" + (monthOfYear + 1) + "-" +dayOfMonth );

                                date = eText.getText().toString().trim();

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });


        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                date = eText.getText().toString().trim();

                Call<List<Produit>> call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .getCodeOnDate(date, idDepot);

                call.enqueue(new Callback<List<Produit>>() {
                    @Override
                    public void onResponse(Call<List<Produit>> call, Response<List<Produit>> response) {

                        produitList.clear();
                        adapter.notifyDataSetChanged();

                        listeCodes = response.body();


                        for (int i = 0; i<listeCodes.size(); i++)
                        {

                            ///////////////// total quantite chargee de chaque produit///////////////////

                            //    InsertDataInList(listeCodes.get(i).code_produit, 6, 6, 6);

                            Call<Integer> call1 = RetrofitClient
                                    .getInstance()
                                    .getApi()
                                    .getSumQuantiteOnDate(listeCodes.get(i).code_produit, date, idDepot);


                            final int finalI = i;
                            call1.enqueue(new Callback<Integer>() {
                                @Override
                                public void onResponse(Call<Integer> call, Response<Integer> response) {

                                    int s = response.body();

                                    final int sumQte = s;


                                    Call<Integer> call2 = RetrofitClient
                                            .getInstance()
                                            .getApi()
                                            .getQteVendueOnDate(listeCodes.get(finalI).code_produit, date);

                                    call2.enqueue(new Callback<Integer>() {
                                        @Override
                                        public void onResponse(Call<Integer> call, Response<Integer> response) {

                                            int s= response.body();

                                            int qteVendue = s;

                                            int qteRest = sumQte - qteVendue;

                                            InsertDataInList(listeCodes.get(finalI).code_produit, listeCodes.get(finalI).nom_produit, sumQte, qteVendue, qteRest);


                                        }


                                        @Override
                                        public void onFailure(Call<Integer> call, Throwable t) {

                                        }
                                    });

                                }

                                @Override
                                public void onFailure(Call<Integer> call, Throwable t) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Produit>> call, Throwable t) {

                    }
                });

            }
        });


    }








    /////////////////Retourner liste des catégories////////////////////////////

    private List<Categorie> getListCategories() {
        try {
            mService.getListCategories().enqueue(new Callback<List<Categorie>>() {
                @Override
                public void onResponse(Call<List<Categorie>> call, Response<List<Categorie>> response) {

                    listeCategories = response.body();
                    ArrayAdapter<Categorie> dataAdapterCategories = new ArrayAdapter<Categorie>(Activity_Inventaire.this, android.R.layout.simple_spinner_item, listeCategories);
                    dataAdapterCategories.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerCategorie.setAdapter(dataAdapterCategories);

                }

                @Override
                public void onFailure(Call<List<Categorie>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception ex) {
            Toast.makeText(Activity_Inventaire.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return listeCategories;
    }



    ////////////////Retourner liste des produits /////////////////////////////
    private List<Produit> getListProduit(int i) {
        mService.getListProduit(i).enqueue(new Callback<List<Produit>>() {
            @Override
            public void onResponse(Call<List<Produit>> call, Response<List<Produit>> response) {

                listeProduits = response.body();
                ArrayAdapter<Produit> dataAdapterProduits = new ArrayAdapter<Produit>(Activity_Inventaire.this, android.R.layout.simple_spinner_item, listeProduits);
                dataAdapterProduits.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerNomP.setAdapter(dataAdapterProduits);

            }

            @Override
            public void onFailure(Call<List<Produit>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        return listeProduits;
    }




    public void InsertDataInList(String code_prod, String nom_prod, int sumQte, int qteVendue, int qteRes) {

                try {

                    final HashMap<String, String> map = new HashMap<String, String>();

                        map.put(KEY_CODE_PRODUIT, code_prod);
                        map.put(KEY_NOM_PRODUIT, nom_prod);
                        map.put(KEY_SUM_Quantite, sumQte+ "");
                        map.put(KEY_QUANTITE_VENDUE, qteVendue + "");
                        map.put(KEY_QUANTITE_RES, qteRes + "");

                        produitList.add(map);
                        list.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                } catch (Exception ex) {
                    Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

    }



  public void LancerInventaire()
  {

      qteInv = qte.getText().toString().trim();



      /////////////////déclaration des variables pour tester l'existance du produit dans la première liste////////////

      boolean testInv = false;

      for (int i=0; i<produitList.size(); i++)
      {
          if (produitList.get(i).get(KEY_CODE_PRODUIT).equals(currentProduit.code_produit))
          {
              testInv= true;
          }
      }




        //////////////déclaration des variables pour tester l'existance du produit

                 boolean test = false;

                 for (int i=0; i<produitList2.size(); i++) {

                     if (produitList2.get(i).get(KEY_CODE_PRODUIT).equals(currentProduit.code_produit))
                       {
                         testExist =i;
                         test= true;
                       }
                 }

                 if (qteInv.length() > 0) {
                      qteP = Integer.valueOf(qteInv);

                      if (qteP > 0) {

                          if (testInv) {

                              try {

                                  String dateDeJour = eText.getText().toString().trim();

                                  Call<Integer> call = RetrofitClient
                                          .getInstance()
                                          .getApi()
                                          .getQuantiteRestanteOnDate(currentProduit.code_produit, dateDeJour, idDepot);

                                  final boolean finalTest = test;

                                  call.enqueue(new Callback<Integer>() {
                                      @Override
                                      public void onResponse(Call<Integer> call, Response<Integer> response) {

                                          int qteRes = response.body();

                                          int ecart = qteP - qteRes;
                                          ecart = Math.abs(ecart);

                                          if (!finalTest) {

                                              HashMap<String, String> map = new HashMap<String, String>();

                                              map.put(KEY_NOM_PRODUIT, currentProduit.nom_produit);
                                              map.put(KEY_CODE_PRODUIT, currentProduit.code_produit);
                                              map.put(KEY_NOM_CATEGORIE, currentCategorie.designation);
                                              map.put(KEY_QUANTITE_RES, qteRes + "");
                                              map.put(KEY_QUANTITE_INV, qteP + "");
                                              map.put(KEY_ECART, ecart + "");

                                              produitList2.add(map);

                                              list2.setAdapter(adapter2);

                                              adapter2.notifyDataSetChanged();

                                              list2.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                                                  @Override
                                                  public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                                                 int pos, long arg3) {
                                                      deleteItem = pos;

                                                      // Creating a new alert dialog to confirm the delete
                                                      AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                                                              .setTitle("Etes vous sûr de vouloir supprimer ce produit")
                                                              .setPositiveButton("Oui",
                                                                      new DialogInterface.OnClickListener() {
                                                                          public void onClick(DialogInterface dialog,
                                                                                              int whichButton) {

                                                                              try {
                                                                                  produitList2.remove(deleteItem);
                                                                                  adapter2.notifyDataSetChanged();
                                                                                  dialog.dismiss();

                                                                              } catch (Exception e) {
                                                                                  Toast.makeText(Activity_Inventaire.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                                              }

                                                                          }
                                                                      })
                                                              .setNegativeButton("Non",
                                                                      new DialogInterface.OnClickListener() {
                                                                          public void onClick(DialogInterface dialog,
                                                                                              int whichButton) {
                                                                              // When you press cancel, just close the
                                                                              // dialog
                                                                              dialog.cancel();
                                                                          }
                                                                      }).show();


                                                      return false;

                                                  }
                                              });

                                          } else {

                                              new AlertDialog.Builder(Activity_Inventaire.this)
                                                      .setTitle("Valider la quantité du produit ")
                                                      .setMessage("Etes vous sûr de vouloir mettre à jour la quantité de ce produit ?")
                                                      .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                                          @Override
                                                          public void onClick(DialogInterface dialog, int which) {

                                                              int q = Integer.parseInt(produitList2.get(testExist).get(KEY_QUANTITE_INV));
                                                              int ecart = Integer.parseInt(produitList2.get(testExist).get(KEY_ECART));
                                                              int sommeEcart = ecart + qteP;
                                                              int sommeQte = q + qteP;

                                                              produitList2.get(testExist).put(KEY_QUANTITE_INV, sommeQte + "");
                                                              produitList2.get(testExist).put(KEY_ECART, sommeEcart + "");

                                                              list2.setAdapter(adapter2);
                                                              adapter2.notifyDataSetChanged();

                                                          }
                                                      })

                                                      .setNegativeButton("Non", null).show();
                                          }

                                      }


                                      @Override
                                      public void onFailure(Call<Integer> call, Throwable t) {

                                      }
                                  });

                              } catch (Exception ex) {
                                  Toast.makeText(Activity_Inventaire.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                              }

                            }
                          else

                          {
                              Toast.makeText(this, "Produit non chargé", Toast.LENGTH_SHORT).show();
                          }


                      } else{
                          Toast.makeText(Activity_Inventaire.this, "Veuillez insérer une valeur positive", Toast.LENGTH_SHORT).show();
                      }
                  } else {
                      Toast.makeText(Activity_Inventaire.this, "Veuillez insérer la quantité", Toast.LENGTH_SHORT).show();
                  }

          }




    public void Clear_Quantite()
    {
        qte.setText("");
    }



}












