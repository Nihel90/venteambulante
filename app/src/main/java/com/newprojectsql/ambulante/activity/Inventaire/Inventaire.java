package com.newprojectsql.ambulante.activity.Inventaire;

public class Inventaire {

    public int id_inventaire;
    public String num_inventaire;
    public String date;
    public int id_depot;

    public Inventaire(int id_inventaire, String num_inventaire, String date, int id_depot) {
        this.id_inventaire = id_inventaire;
        this.num_inventaire = num_inventaire;
        this.date = date;
        this.id_depot = id_depot;
    }


    public int getId_inventaire() {
        return id_inventaire;
    }

    public String getNum_inventaire() {
        return num_inventaire;
    }

    public String getDate() {
        return date;
    }

    public int getId_depot() {
        return id_depot;
    }


    public void setId_inventaire(int id_inventaire) {
        this.id_inventaire = id_inventaire;
    }

    public void setNum_inventaire(String num_inventaire) {
        this.num_inventaire = num_inventaire;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId_depot(int id_depot) {
        this.id_depot = id_depot;
    }
}
