package com.newprojectsql.ambulante.activity.GestionClients;

import java.io.Serializable;

public class Adresse implements Serializable {

    public int id_adr_client ;
    public String adresse ;
    public String gouvernorat ;
    public String type ;
    public String ville ;
    public int id_client ;




    public Adresse(int id_adr_client, String adresse, String gouvernorat, String type, String ville, int id_client) {
        this.id_adr_client = id_adr_client;
        this.adresse = adresse;
        this.gouvernorat = gouvernorat;
        this.type = type;
        this.ville = ville;
        this.id_client = id_client;
    }


    public Adresse(String adresse, String gouvernorat, String ville)
    {
        this.adresse= adresse;
        this.gouvernorat = gouvernorat;
        this.ville = ville;
    }


    public Adresse()
    {}

    public int getId_adr_client() {
        return id_adr_client;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getGouvernorat() {
        return gouvernorat;
    }

    public String getType() {
        return type;
    }

    public String getVille() {
        return ville;
    }

    public int getId_client() {
        return id_client;
    }


    public void setId_adr_client(int id_adr_client) {
        this.id_adr_client = id_adr_client;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setGouvernorat(String gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }


    @Override
    public String toString() {
        return adresse ;

    }



}
