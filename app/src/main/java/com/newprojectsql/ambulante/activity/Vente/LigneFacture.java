package com.newprojectsql.ambulante.activity.Vente;

import java.io.Serializable;

public class LigneFacture implements Serializable {

    public int id_ligne_facture;
    public int id_facture;
    public String code_produit, nom_produit;
    public Double prix_total, prix_unitaire, prix_ttc, prix_ttc_total;
    public int qte, tva;
    public String type_remise, unite;
    public int val_remise;


    public void  LigneFacture()
    {

    }

    public Double getPrix_ttc() {
        return prix_ttc;
    }

    public Double getPrix_ttc_total() {
        return prix_ttc_total;
    }

    public int getTva() {
        return tva;
    }

    public void setPrix_ttc(Double prix_ttc) {
        this.prix_ttc = prix_ttc;
    }

    public void setPrix_ttc_total(Double prix_ttc_total) {
        this.prix_ttc_total = prix_ttc_total;
    }

    public void setTva(int tva) {
        this.tva = tva;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public int getId_ligne_facture() {
        return id_ligne_facture;
    }

    public int getId_facture() {
        return id_facture;
    }

    public String getCode_produit() {
        return code_produit;
    }

    public Double getPrix_total() {
        return prix_total;
    }

    public Double getPrix_unitaire() {
        return prix_unitaire;
    }

    public int getQte() {
        return qte;
    }

    public String getType_remise() {
        return type_remise;
    }

    public String getUnite() {
        return unite;
    }

    public int getVal_remise() {
        return val_remise;
    }


    public void setId_ligne_facture(int id_ligne_facture) {
        this.id_ligne_facture = id_ligne_facture;
    }

    public void setId_facture(int id_facture) {
        this.id_facture = id_facture;
    }

    public void setCode_produit(String code_produit) {
        this.code_produit = code_produit;
    }

    public void setPrix_total(Double prix_total) {
        this.prix_total = prix_total;
    }

    public void setPrix_unitaire(Double prix_unitaire) {
        this.prix_unitaire = prix_unitaire;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public void setType_remise(String type_remise) {
        this.type_remise = type_remise;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public void setVal_remise(int val_remise) {
        this.val_remise = val_remise;
    }



}
