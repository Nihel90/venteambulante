package com.newprojectsql.ambulante.activity.Inventaire;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.Produit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListProd2_Inv extends BaseAdapter {

    private List<Produit> listProduit;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListProd2_Inv(List<Produit> listProduit) {
        this.listProduit = listProduit;
    }



    public AdapterListProd2_Inv(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.listprod2_inventaire, null);


        TextView categorie = (TextView)vi.findViewById(R.id.categorie);
        TextView code_produit = (TextView)vi.findViewById(R.id.codeP);
        TextView quantite_inv = (TextView)vi.findViewById(R.id.qte_inv);
        TextView quantite_phys = (TextView)vi.findViewById(R.id.qte_physique);
        TextView nom_produit = (TextView)vi.findViewById(R.id.nom_produit);
        TextView ecart = (TextView)vi.findViewById(R.id.ecart);


        HashMap<String, String> produit = new HashMap<String, String>();
        produit = data.get(position);

        // Setting all values in listview

        categorie.setText(produit.get(Activity_Inventaire.KEY_NOM_CATEGORIE));
        code_produit.setText(produit.get(Activity_Inventaire.KEY_CODE_PRODUIT));
        nom_produit.setText(produit.get(Activity_Inventaire.KEY_NOM_PRODUIT));
        quantite_inv.setText(produit.get(Activity_Inventaire.KEY_QUANTITE_INV));
        quantite_phys.setText(produit.get(Activity_Inventaire.KEY_QUANTITE_RES));
        ecart.setText(produit.get(Activity_Inventaire.KEY_ECART));


        return vi;
    }







}
