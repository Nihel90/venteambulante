package com.newprojectsql.ambulante.activity.Vente;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.newprojectsql.ambulante.activity.GestionClients.Adresse;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.Client;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Reglement.Activity_Reglement;
import com.newprojectsql.ambulante.activity.Reglement.Reglement;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.DoubleToLongFunction;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Valider_Vente extends Activity {

    /////Declaration Api
    Api mService;

    ///// Declaration des Views

    private List<Adresse> listeAdressesLiv;
    private List<Adresse> listeAdressesFac;

    private Spinner spinnerAdrresseLiv, spinnerAdrresseFac;
    private Button  btn_cloturer, btn_valider;
    private TextView txt_timbre, txt_prix_ttc;
    private TextView txt_total_prix_ttc;
    private EditText remise;


    //////////////////////////////////////////////////
   ///////////////////////////////////////////////////
    static final String KEY_NOM_PRODUIT = "nomP";
    static final String KEY_CODE_PRODUIT = "codeP";
    static final String KEY_PRIX_U = "prixU";
    static final String KEY_QUANTITE = "quantite";
    static final String KEY_PRIX_TOTAL = "prixTotal";
    static final String KEY_PRIX_TTC = "prix_ttc";
    static final String KEY_PRIX_TTC_TOTAL = "prix_ttc_total";
    static final String KEY_TVA = "tva";

    ///////////////////////////////////////////////////
    /////////////////////////////////////////////////

    public static Adresse currentAdresse = null;

    int id_adr_client;
    Double valRemise=0.0, tva=0.0, timbre, prix_ttc=0.0, prix_ttc_total=0.0;
    String adresseFac, adresseLiv;

    public Facture facture;
    public Client client;
    ArrayList<HashMap<String, String>> lisProd;
    public LigneFacture currentLigneFacture ;


    int id_depot =1;
    int nouvQte =0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valider_vente);


        mService = RetrofitClient.getInstance().getApi();


        // Spinner element
        spinnerAdrresseLiv = (Spinner) findViewById(R.id.spinnerAdr_liv);
        spinnerAdrresseFac = (Spinner) findViewById(R.id.spinnerAdr_fac);


        txt_timbre = (TextView) findViewById(R.id.txt_timbre);
        txt_prix_ttc = (TextView) findViewById(R.id.txt_prix_ttc);
        txt_total_prix_ttc = (TextView) findViewById(R.id.txt_total_prix_ttc);

        remise = (EditText) findViewById(R.id.remise);



        btn_valider = (Button) findViewById(R.id.btn_valider);

        btn_cloturer = (Button) findViewById(R.id.btn_cloturer);


        //////////////////////////Intent Get object Facture and List of products//////////////////////////

        facture = (Facture) getIntent().getSerializableExtra("FACTURE");
        client = (Client) getIntent().getSerializableExtra("CLIENT");


        lisProd =  (ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra("LIGNESFACTURE");



        txt_prix_ttc.setText(facture.prix_ttc.toString());
        prix_ttc_total = facture.prix_ttc + 0.6;
        prix_ttc_total =  Math.round(prix_ttc_total*1000.0)/1000.0;
        txt_total_prix_ttc.setText(prix_ttc_total.toString());
        remise.setText(valRemise.toString());


        ////////////////////////////////////////////////////
       /////////////////////  Déclaration des listes///////////
        listeAdressesLiv = getAddressLiv(facture.id_client);
        listeAdressesFac = getAddressFac(facture.id_client);


        currentLigneFacture = new LigneFacture();



        //////////////get Value of a timbre //////////////////////
        //////////////////////////////////////////////////////////

        Call<Double> call = RetrofitClient
                .getInstance()
                .getApi()
                .getTimbre();

        call.enqueue(new Callback<Double>() {
            @Override
            public void onResponse(Call<Double> call, Response<Double> response) {

                double s= response.body();
                timbre = s;

                txt_timbre.setText(timbre.toString());

            }

            @Override
            public void onFailure(Call<Double> call, Throwable t) {

            }
        });





   // Spinner click listener
        spinnerAdrresseLiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //On selected a spinner Item

                currentAdresse = listeAdressesLiv.get(position);
                adresseLiv = currentAdresse.adresse;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Spinner click listener
        spinnerAdrresseFac.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //On selected a spinner Item
                currentAdresse = listeAdressesFac.get(position);
                adresseFac = currentAdresse.adresse;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ///////////////////////////
        ///////////////////////////




       btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              prix_ttc_total =  Get_Total_Prix_TTC();
              txt_total_prix_ttc.setText(prix_ttc_total.toString());
            }

        });


         btn_cloturer.setOnClickListener(new View.OnClickListener() {
            @Override
             public void onClick(View v) {


          if (prix_ttc_total!=0.0) {

              new AlertDialog.Builder(Valider_Vente.this)
                      .setTitle("Cloture Vente")
                      .setMessage("Etes vous sûr de vouloir clôturer la vente ?")
                      .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {

                              ////ajouter facture et lignes de facture/////
                              AddFacture();

                              getDetailsFacture();

                            // Intent activity = new Intent(Valider_Vente.this, Reglement.class);
                            //  startActivity(activity);
                          }
                      }).setNegativeButton("Non", null).show();


          }

          else
          {
              Toast.makeText(Valider_Vente.this, "Veuillez valider le prix TTC", Toast.LENGTH_SHORT).show();
          }

         }

         });



            }


    public void onItemSelected(AdapterView parent, View view, int position, long id) {

    }


    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }


    public void test(View view) {

        prix_ttc_total =  Get_Total_Prix_TTC();
        txt_total_prix_ttc.setText(prix_ttc_total.toString());

        Toast.makeText(this, "heloooooo", Toast.LENGTH_SHORT).show();
    }



    public Double Get_Total_Prix_TTC()
    {
        Double total_prix_ttc = 0.0;
        String val= remise.getText().toString().trim();


            valRemise = Double.valueOf(val);

            if ((valRemise >= 0.0)&& (valRemise < facture.prix_ttc)) {


               total_prix_ttc = facture.prix_ttc - valRemise ;
               total_prix_ttc = total_prix_ttc + timbre;
               total_prix_ttc= Math.round(total_prix_ttc*1000.0)/1000.0;

            }
            else
            {
                Toast.makeText(this, "Veuillez insérer une valeure de remise valide", Toast.LENGTH_SHORT).show();
                total_prix_ttc = facture.prix_ttc + timbre;
                total_prix_ttc = Math.round(total_prix_ttc*1000.0)/1000.0;
            }


        return total_prix_ttc;
    }




   private List<Adresse> getAddressFac(int id) {
        mService.getAddressFac(id).enqueue(new Callback<List<Adresse>>() {
            @Override
            public void onResponse(Call<List<Adresse>> call, Response<List<Adresse>> response) {

                listeAdressesFac = response.body();
                ArrayAdapter<Adresse> dataAdapterAdresse = new ArrayAdapter<Adresse>(Valider_Vente.this, android.R.layout.simple_spinner_item, listeAdressesFac);
                dataAdapterAdresse.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerAdrresseFac.setAdapter(dataAdapterAdresse);

            }

            @Override
            public void onFailure(Call<List<Adresse>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        return listeAdressesFac;

    }



    private List<Adresse> getAddressLiv(int id) {
        mService.getAddressLiv(id).enqueue(new Callback<List<Adresse>>() {
            @Override
            public void onResponse(Call<List<Adresse>> call, Response<List<Adresse>> response) {

                listeAdressesLiv = response.body();
                ArrayAdapter<Adresse> dataAdapterAdresse = new ArrayAdapter<Adresse>(Valider_Vente.this, android.R.layout.simple_spinner_item, listeAdressesLiv);
                dataAdapterAdresse.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerAdrresseLiv.setAdapter(dataAdapterAdresse);
            }

            @Override
            public void onFailure(Call<List<Adresse>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    return listeAdressesLiv;
    }



     private void AddFacture() {

         Toast.makeText(this, ""+timbre, Toast.LENGTH_SHORT).show();
          facture.setPrix_ttc_total(prix_ttc_total);
          facture.setTimbre_f(timbre);
          facture.setRemise(valRemise);
          facture.setAdresse_fac(adresseFac);
          facture.setAdresse_liv(adresseLiv);


          Call<ResponseBody> call = RetrofitClient
                  .getInstance()
                  .getApi()
                  .createFacture(facture.num_facture, facture.id_client, facture.raison_sociale, facture.prix_ht, facture.prix_ttc, facture.prix_ttc_total, adresseFac, adresseLiv, facture.remise, facture.timbre_f);


          call.enqueue(new Callback<ResponseBody>() {
              @Override
              public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                  String s = null;

                  try {

                      s = response.body().string();
                      String h = s.replace("\"", "");

                      if (h.equals("num facture existe deja, veuillez verifier de nouveau")) {
                          Toast.makeText(Valider_Vente.this, s, Toast.LENGTH_LONG).show();


                      } else if (h.equals("Echec d insertion")) {
                          Toast.makeText(Valider_Vente.this, s, Toast.LENGTH_LONG).show();
                      }
                      else {
                          facture.id_facture = Integer.parseInt(h);

                          ///////Appel à la la méthode AddLigneFacture pour ajouter les lignes facture dont id_facture = id_facture
                          AddLigneFacture(facture.id_facture);

                      }

                  } catch (IOException e) {
                      e.printStackTrace();
                  }

              }


              @Override
              public void onFailure(Call<ResponseBody> call, Throwable t) {

                  Toast.makeText(Valider_Vente.this, t.getMessage(), Toast.LENGTH_LONG).show();

              }
          });
      }



      private void AddLigneFacture(final int idFacture) {

          for (int i = 0; i < lisProd.size(); i++) {
              currentLigneFacture.code_produit = lisProd.get(i).get(KEY_CODE_PRODUIT);
              currentLigneFacture.nom_produit = lisProd.get(i).get(KEY_NOM_PRODUIT);
              currentLigneFacture.prix_total = Double.valueOf(lisProd.get(i).get(KEY_PRIX_TOTAL));
              currentLigneFacture.prix_unitaire = Double.valueOf(lisProd.get(i).get(KEY_PRIX_U));
              currentLigneFacture.qte = Integer.parseInt(lisProd.get(i).get(KEY_QUANTITE));
              currentLigneFacture.prix_ttc = Double.valueOf(lisProd.get(i).get(KEY_PRIX_TTC));
              currentLigneFacture.prix_ttc_total = Double.valueOf(lisProd.get(i).get(KEY_PRIX_TTC_TOTAL));
              currentLigneFacture.tva = Integer.parseInt(lisProd.get(i).get(KEY_TVA));


                     /*Do add Lignefacture using the api call*/
                     Call<ResponseBody> call3 = RetrofitClient
                             .getInstance()
                             .getApi()
                             .createLigneFacture(currentLigneFacture.code_produit, currentLigneFacture.nom_produit,  currentLigneFacture.prix_total,  currentLigneFacture.prix_unitaire,  currentLigneFacture.qte, idFacture, currentLigneFacture.prix_ttc, currentLigneFacture.prix_ttc_total, currentLigneFacture.tva, id_depot);

                     call3.enqueue(new Callback<ResponseBody>() {
                         @Override
                         public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                             String s = null;

                             try {

                                 s = response.body().string();
                                 String h = s.replace("\"", "");

                                 Toast.makeText(Valider_Vente.this, "" + s, Toast.LENGTH_SHORT).show();


                             } catch (IOException e) {
                                 e.printStackTrace();
                             }

                         }

                         @Override
                         public void onFailure(Call<ResponseBody> call, Throwable t) {

                         }
                     });

        }
    }





    public void getDetailsFacture()
    {
        Intent intent1 = new Intent(Valider_Vente.this, Activity_Reglement.class);

        Facture fact = new Facture (facture.num_facture, facture.id_client, facture.prix_ht, facture.remise, facture.timbre_f, facture.prix_ttc_total);


        ArrayList<HashMap<String, String>> produitList2 = new ArrayList<>();
        for (int i=0; i<lisProd.size(); i++)
        {
            produitList2.add(lisProd.get(i));

        }

        intent1.putExtra("FACTURE", fact);
        intent1.putExtra("LIGNESFACTURE", produitList2);

        Valider_Vente.this.startActivity(intent1);

    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }








}