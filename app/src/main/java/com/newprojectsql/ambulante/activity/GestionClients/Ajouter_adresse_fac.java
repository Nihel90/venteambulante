package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Chargement.Activity_Chargement;
import com.newprojectsql.ambulante.activity.Vente.Facture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ajouter_adresse_fac extends AppCompatActivity {

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutAdresse;
    private TextInputLayout textInputLayoutGouvernorat;
    private TextInputLayout textInputLayoutVille;



    private TextInputEditText textInputEditTextAdresse;
    private TextInputEditText textInputEditTextGouvernorat;
    private TextInputEditText textInputEditTextVille;




    private AppCompatButton appCompatButtonRegister;
    private Button btn_valider;


    public Client currentClient;
    public  ArrayList<HashMap<String, String>> listAdresseLiv;
    public String adresse, gouvernorat, ville;

    //////////////////////////list view des adresses //////////////////////
    ///////////////////////////////////////////////////////////////////////
    static final String KEY_ADRESSE = "adresse";
    static final String KEY_GOUVERNORAT = "gouvernorat";
    static final String KEY_VILLE = "ville";

    private ListView list;
    private  Adresse current_address_liv, current_address_fac;

    String adr, gouv, v;


    private TextView adresseLiv;
    private TextView gouvLiv;
    private TextView villeLiv;

    AdapterListAdrLiv adapter;

    List<Adresse> lAdresseLiv;
    ArrayList<HashMap<String, String>> adresseFacList;
    Adresse currentAdresse = null;
    // contains the id of the item we are about to delete
    public int deleteItem;

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_adresse_fac);


        /////////////////////Liste adresses ///////////////
        ///////////////////////////////////////////////////

        adresseLiv = (TextView) findViewById(R.id.adresse);
        gouvLiv = (TextView) findViewById(R.id.gouvernorat);
        villeLiv = (TextView) findViewById(R.id.ville);


        current_address_liv = new Adresse();
        current_address_fac = new Adresse();



        //////////////////list view///////////
        list = (ListView) findViewById(R.id.list);

        //////////////////////////////////////////////////////
        adresseFacList = new ArrayList<HashMap<String, String>>();

        adapter = new AdapterListAdrLiv(Ajouter_adresse_fac.this, adresseFacList);

        ////////////////////list view /////////////





        textInputLayoutAdresse = (TextInputLayout) findViewById(R.id.textInputLayoutAdresse);
        textInputLayoutGouvernorat = (TextInputLayout) findViewById(R.id.textInputLayoutGouvernorat);
        textInputLayoutVille = (TextInputLayout) findViewById(R.id.textInputLayoutVille);



        textInputEditTextAdresse = (TextInputEditText) findViewById(R.id.textInputEditTextAdresse);
        textInputEditTextGouvernorat = (TextInputEditText) findViewById(R.id.textInputEditTextGouvernorat);
        textInputEditTextVille = (TextInputEditText) findViewById(R.id.textInputEditTextVille);



        //////////Déclaration des listes ///////////////
        currentClient= (Client)getIntent().getSerializableExtra("CLIENT");
        listAdresseLiv =  (ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra("LIGNESFACTURE");







        // Create button
        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);
        btn_valider = (Button)findViewById(R.id.btn_valider);



        btn_valider.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ValiderAjout();

            }
        });


        appCompatButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuitterAjoutAdresseLiv();
            }
        });

    }


    private void QuitterAjoutAdresseLiv() {

        new AlertDialog.Builder(this)
                .setTitle("Valider les adresses de facturation")
                .setMessage("Etes vous sûr de vouloir ajouter ces adresses de facturation ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        AddClient();
                        Intent activity = new Intent(Ajouter_adresse_fac.this, Chercher_client.class);
                        startActivity(activity);
                        finish();


                    }
                }).setNegativeButton("Non", null).show();
    }



    private void emptyInputEditText() {
        textInputEditTextAdresse.setText(null);
        textInputEditTextVille.setText(null);
        textInputEditTextGouvernorat.setText(null);
        textInputEditTextAdresse.requestFocus();

    }


    private void ValiderAjout()
    {
        adr= textInputEditTextAdresse.getText().toString().trim();
        gouv= textInputEditTextGouvernorat.getText().toString().trim();
        v= textInputEditTextVille.getText().toString().trim();




        if (adr.isEmpty()){
            textInputEditTextAdresse.setError("Le champs 'Adresse' est obligatoire");
            textInputEditTextAdresse.requestFocus();
            emptyInputEditText();
            return;
        }


        if (gouv.isEmpty()){
            textInputEditTextGouvernorat.setError("Le champs 'Gouvernorat' est obligatoire");
            textInputEditTextGouvernorat.requestFocus();
            return;
        }

        if (v.isEmpty()){
            textInputEditTextVille.setError("Le champs 'Ville' est obligatoire");
            textInputEditTextVille.requestFocus();
            return;
        }

        InsertDataInList();
        emptyInputEditText();
    }


    public void AddClient()
    {

        /*Do user Registration using the api call*/

        Call<ResponseBody> call= RetrofitClient
                .getInstance()
                .getApi()
                .CreateClient(currentClient.code_client, currentClient.raison_sociale, currentClient.matricule_fiscale,
                        currentClient.registre_commerce, currentClient.email, currentClient.tel_mobile, currentClient.tel_fixe,
                        currentClient.latitude, currentClient.longitude, currentClient.max_credit);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                 String s = null;

                 try {

                     s = response.body().string();
                     String h = s.replace("\"", "");

                     if (h.equals("raison_sociale existe deja, veuillez verifier de nouveau")) {
                         Toast.makeText(Ajouter_adresse_fac.this, h, Toast.LENGTH_LONG).show();



                     } else if (h.equals("Echec d insertion")) {
                         Toast.makeText(Ajouter_adresse_fac.this, s, Toast.LENGTH_LONG).show();


                     }

                     else {

                         currentClient.id_client =Integer.parseInt(h);
                         AddAddressLiv(currentClient.id_client);
                         AddAddressFac(currentClient.id_client);


                     }


                       } catch(IOException e){
                           e.printStackTrace();
                 }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Ajouter_adresse_fac.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }




    public void AddAddressLiv(int idClient)
    {

        for (int i=0; i<listAdresseLiv.size(); i++) {

            current_address_liv.adresse = listAdresseLiv.get(i).get(KEY_ADRESSE);
            current_address_liv.gouvernorat = listAdresseLiv.get(i).get(KEY_GOUVERNORAT);
            current_address_liv.ville = listAdresseLiv.get(i).get(KEY_VILLE);



            /*Do address Registration using the api call*/
            Call<ResponseBody> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .CreateAddressLiv(current_address_liv.adresse, current_address_liv.gouvernorat, current_address_liv.ville, idClient);


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        String s = response.body().string();
                        emptyInputEditText();
                        Toast.makeText(Ajouter_adresse_fac.this, s, Toast.LENGTH_LONG).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(Ajouter_adresse_fac.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }

            });

        }


    }



    public void AddAddressFac(int idClient)
    {
        for (int i=0; i<adresseFacList.size(); i++) {

            current_address_fac.adresse = adresseFacList.get(i).get(KEY_ADRESSE);
            current_address_fac.gouvernorat = adresseFacList.get(i).get(KEY_GOUVERNORAT);
            current_address_fac.ville = adresseFacList.get(i).get(KEY_VILLE);



            /*Do address Registration using the api call*/
            Call<ResponseBody> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .CreateAddressFac(current_address_fac.adresse, current_address_fac.gouvernorat, current_address_fac.ville, idClient);


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        String s = response.body().string();
                        emptyInputEditText();
                        Toast.makeText(Ajouter_adresse_fac.this, s, Toast.LENGTH_LONG).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Toast.makeText(Ajouter_adresse_fac.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }

            });

        }





    }





    public void InsertDataInList()
    {

        try {

            HashMap<String, String> map = new HashMap<String, String>();

            map.put(KEY_ADRESSE, adr);
            map.put(KEY_GOUVERNORAT, gouv);
            map.put(KEY_VILLE, v);


            adresseFacList.add(map);

            list.setAdapter(adapter);

            adapter.notifyDataSetChanged();


            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long arg3) {
                    deleteItem = pos;
                    // Creating a new alert dialog to confirm the delete
                    AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                            .setTitle("Etes vous sûr de vouloir supprimer cette adresse ?")
                            .setPositiveButton("Oui",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {

                                            try {
                                                adresseFacList.remove(deleteItem);

                                                adapter.notifyDataSetChanged();
                                                dialog.dismiss();


                                            } catch (Exception e) {
                                                Toast.makeText(Ajouter_adresse_fac.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    })
                            .setNegativeButton("Non",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {
                                            // When you press cancel, just close the
                                            // dialog
                                            dialog.cancel();
                                        }
                                    }).show();


                    return false;

                }
            });


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }


}
