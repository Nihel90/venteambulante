package com.newprojectsql.ambulante.activity.Reglement;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListReglement extends BaseAdapter {

    private List<Reglement> listReglement;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListReglement(List<Reglement> listReglement) {
        this.listReglement = listReglement;
    }



    public AdapterListReglement(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_reglement, null);

        TextView txt_num_reglement = (TextView)vi.findViewById(R.id.txt_num_reglement);
        TextView txt_type_reglement = (TextView)vi.findViewById(R.id.txt_type_reglement); //
        TextView txt_montant = (TextView)vi.findViewById(R.id.txt_montant); //



        HashMap<String, String> reglement = new HashMap<String, String>();
        reglement = data.get(position);

        // Setting all values in listview
        txt_num_reglement.setText(reglement.get(Activity_Reglement.KEY_NUM_REGLEMENT));
        txt_type_reglement.setText(reglement.get(Activity_Reglement.KEY_TYPE_REGLEMENT));
        txt_montant.setText(reglement.get(Activity_Reglement.KEY_MONTANT));


        return vi;
    }


}
