package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.AdapterListProduit;
import com.newprojectsql.ambulante.activity.Vente.Facture;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Valider_Vente;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ajouter_adresse extends AppCompatActivity {


    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutAdresse;
    private TextInputLayout textInputLayoutGouvernorat;
    private TextInputLayout textInputLayoutVille;



    private TextInputEditText textInputEditTextAdresse;
    private TextInputEditText textInputEditTextGouvernorat;
    private TextInputEditText textInputEditTextVille;




    private AppCompatButton appCompatButtonRegister;
    private Button btn_valider;


    public Client clt;
    public Client currentClient;
    public String adresse, gouvernorat, ville;

    //////////////////////////list view des adresses //////////////////////
    ///////////////////////////////////////////////////////////////////////
    static final String KEY_ADRESSE = "adresse";
    static final String KEY_GOUVERNORAT = "gouvernorat";
    static final String KEY_VILLE = "ville";

    private ListView list;
    private  Adresse adresse_liv;

    String adr, gouv, v;


    private TextView adresseLiv;
    private TextView gouvLiv;
    private TextView villeLiv;

    AdapterListAdrLiv adapter;

    List<Adresse> lAdresseLiv;
    ArrayList<HashMap<String, String>> adresseLivList;
    Adresse currentAdresse = null;
    // contains the id of the item we are about to delete
    public int deleteItem;

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_adresse_liv);


        /////////////////////Liste adresses ///////////////
        ///////////////////////////////////////////////////

        adresseLiv = (TextView) findViewById(R.id.adresse);
        gouvLiv = (TextView) findViewById(R.id.gouvernorat);
        villeLiv = (TextView) findViewById(R.id.ville);


        //////////////////list view///////////
        adresse_liv = new Adresse();

        list = (ListView) findViewById(R.id.list);

        //////////////////////////////////////////////////////
        adresseLivList = new ArrayList<HashMap<String, String>>();
     //   listeLigneFacture = new ArrayList<HashMap<String, String>>();

        adapter = new AdapterListAdrLiv(Ajouter_adresse.this, adresseLivList);

       ////////////////////list view /////////////





        textInputLayoutAdresse = (TextInputLayout) findViewById(R.id.textInputLayoutAdresse);
        textInputLayoutGouvernorat = (TextInputLayout) findViewById(R.id.textInputLayoutGouvernorat);
        textInputLayoutVille = (TextInputLayout) findViewById(R.id.textInputLayoutVille);



        textInputEditTextAdresse = (TextInputEditText) findViewById(R.id.textInputEditTextAdresse);
        textInputEditTextGouvernorat = (TextInputEditText) findViewById(R.id.textInputEditTextGouvernorat);
        textInputEditTextVille = (TextInputEditText) findViewById(R.id.textInputEditTextVille);



        // Create button
        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);
        btn_valider = (Button)findViewById(R.id.btn_valider);


        btn_valider.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ValiderAjout();
            }
        });


        appCompatButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuitterAjoutAdresseLiv();
            }
        });

    }


    private void QuitterAjoutAdresseLiv() {

        new AlertDialog.Builder(this)
                .setTitle("Valider les adresses de livraison")
                .setMessage("Etes vous sûr de vouloir valider ces adresses de livraison")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                      //  Intent activity = new Intent(Ajouter_adresse.this, Ajouter_adresse_fac.class);
                      //  startActivity(activity);


                        clt= (Client)getIntent().getSerializableExtra("CLIENT");
                        /////pass a client in intent

                        Intent activity = new Intent(Ajouter_adresse.this, Ajouter_adresse_fac.class);

                        //To pass:
                        // in Activity A, when starting Activity B

                        currentClient= new Client (clt.code_client,clt.raison_sociale,
                                clt.matricule_fiscale, clt.registre_commerce, clt.email,
                                clt.tel_mobile, clt.tel_fixe, clt.latitude, clt.longitude, clt.max_credit);



                        ArrayList<HashMap<String, String>> adresseLivList2 = new ArrayList<>();
                        for (int i=0; i<adresseLivList.size(); i++)
                        {
                            adresseLivList2.add(adresseLivList.get(i));
                        }


                        activity.putExtra("LIGNESFACTURE", adresseLivList2);
                        activity.putExtra("CLIENT", currentClient);
                        Ajouter_adresse.this.startActivity(activity);



                    }
                }).setNegativeButton("Non", null).show();
    }



    private void emptyInputEditText() {
        textInputEditTextAdresse.setText(null);
        textInputEditTextVille.setText(null);
        textInputEditTextGouvernorat.setText(null);
        textInputEditTextAdresse.requestFocus();

    }






    private void ValiderAjout()
    {
         adr= textInputEditTextAdresse.getText().toString().trim();
         gouv= textInputEditTextGouvernorat.getText().toString().trim();
         v= textInputEditTextVille.getText().toString().trim();




        if (adr.isEmpty()){
            textInputEditTextAdresse.setError("Le champs 'Adresse' est obligatoire");
            textInputEditTextAdresse.requestFocus();
            return;
        }


        if (gouv.isEmpty()){
            textInputEditTextGouvernorat.setError("Le champs 'Gouvernorat' est obligatoire");
            textInputEditTextGouvernorat.requestFocus();
            return;
        }

        if (v.isEmpty()){
            textInputEditTextVille.setError("Le champs 'Ville' est obligatoire");
            textInputEditTextVille.requestFocus();
            return;
        }






        InsertDataInList();
        emptyInputEditText();


    }



    public void InsertDataInList()
    {

        try {


            HashMap<String, String> map = new HashMap<String, String>();

            map.put(KEY_ADRESSE, adr);
            map.put(KEY_GOUVERNORAT, gouv);
            map.put(KEY_VILLE, v);


            adresseLivList.add(map);

            list.setAdapter(adapter);

            adapter.notifyDataSetChanged();


            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long arg3) {
                    deleteItem = pos;
                    // Creating a new alert dialog to confirm the delete
                    AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                            .setTitle("Etes vous sûr de vouloir supprimer cette adresse")
                            .setPositiveButton("Oui",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {

                                            try {
                                                adresseLivList.remove(deleteItem);

                                                adapter.notifyDataSetChanged();
                                                dialog.dismiss();


                                            } catch (Exception e) {
                                                Toast.makeText(Ajouter_adresse.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    })
                            .setNegativeButton("Non",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {
                                            // When you press cancel, just close the
                                            // dialog
                                            dialog.cancel();
                                        }
                                    }).show();




                    return false;

                }
            });


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }



    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }




}



