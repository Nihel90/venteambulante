package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListAdrLiv extends BaseAdapter {

    private List<Adresse> listAdresseLiv;
    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListAdrLiv(List<Adresse> listAdresseLiv) {
        this.listAdresseLiv = listAdresseLiv;
    }



    public AdapterListAdrLiv(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_address, null);

        TextView adresse = (TextView)vi.findViewById(R.id.adresse); //
        TextView gouvernorat = (TextView)vi.findViewById(R.id.gouvernorat); //
        TextView ville = (TextView)vi.findViewById(R.id.ville); //


        HashMap<String, String> produit = new HashMap<String, String>();
        produit = data.get(position);

        // Setting all values in listview
        adresse.setText(produit.get(Ajouter_adresse.KEY_ADRESSE));
        gouvernorat.setText(produit.get(Ajouter_adresse.KEY_GOUVERNORAT));
        ville.setText(produit.get(Ajouter_adresse.KEY_VILLE));

        return vi;
    }


}
