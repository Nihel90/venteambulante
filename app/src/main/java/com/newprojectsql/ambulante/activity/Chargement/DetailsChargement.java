package com.newprojectsql.ambulante.activity.Chargement;

public class DetailsChargement {

    public int id_detailsC;
    public String code_produit;
    public int quantite;
    public int id_chargement;


    public DetailsChargement() {
    }

    public DetailsChargement(int id_detailsC, String code_produit, int quantite, int id_chargement) {
        this.id_detailsC = id_detailsC;
        this.code_produit = code_produit;
        this.quantite = quantite;
        this.id_chargement = id_chargement;
    }

    public int getId_detailsC() {
        return id_detailsC;
    }

    public String getCode_produit() {
        return code_produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public int getId_chargement() {
        return id_chargement;
    }


    public void setId_detailsC(int id_detailsC) {
        this.id_detailsC = id_detailsC;
    }


    public void setCode_produit(String code_produit) {
        this.code_produit = code_produit;
    }


    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }


    public void setId_chargement(int id_chargement) {
        this.id_chargement = id_chargement;
    }
}
