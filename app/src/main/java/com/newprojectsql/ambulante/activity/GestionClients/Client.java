package com.newprojectsql.ambulante.activity.GestionClients;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Client implements Serializable {

    public int id_client ;
    public String code_client ;
    public String raison_sociale ;
    public String matricule_fiscale ;
    public String registre_commerce ;
    public String email ;
    public String tel_mobile ;
    public String tel_fixe ;
    public Double latitude ;
    public Double longitude ;
    public Double max_credit;


    private List<Client> clients;

    public Client(int id_client, String code_client, String raison_sociale, String matricule_fiscale, String registre_commerce, String email, String tel_mobile, String tel_fixe, Double latitude, Double longitude) {
        this.id_client = id_client;
        this.code_client = code_client;
        this.raison_sociale = raison_sociale;
        this.matricule_fiscale = matricule_fiscale;
        this.registre_commerce = registre_commerce;
        this.email = email;
        this.tel_mobile = tel_mobile;
        this.tel_fixe = tel_fixe;
        this.latitude = latitude;
        this.longitude = longitude;

    }

    public Client(int id_client, String code_client, String raison_sociale, String matricule_fiscale, String registre_commerce, String email, String tel_mobile, String tel_fixe, Double latitude, Double longitude, Double max_credit) {
        this.id_client = id_client;
        this.code_client = code_client;
        this.raison_sociale = raison_sociale;
        this.matricule_fiscale = matricule_fiscale;
        this.registre_commerce = registre_commerce;
        this.email = email;
        this.tel_mobile = tel_mobile;
        this.tel_fixe = tel_fixe;
        this.latitude = latitude;
        this.longitude = longitude;
        this.max_credit = max_credit;

    }


    public Client( String code_client, String raison_sociale, String matricule_fiscale, String registre_commerce, String email, String tel_mobile, String tel_fixe, Double latitude, Double longitude, Double max_credit){
        this.code_client = code_client;
        this.raison_sociale = raison_sociale;
        this.matricule_fiscale = matricule_fiscale;
        this.registre_commerce = registre_commerce;
        this.email = email;
        this.tel_mobile = tel_mobile;
        this.tel_fixe = tel_fixe;
        this.latitude = latitude;
        this.longitude = longitude;
        this.max_credit = max_credit;

    }

    public Client( String code_client, String raison_sociale, String matricule_fiscale, String registre_commerce, String email, String tel_mobile, String tel_fixe, Double latitude, Double longitude){
        this.code_client = code_client;
        this.raison_sociale = raison_sociale;
        this.matricule_fiscale = matricule_fiscale;
        this.registre_commerce = registre_commerce;
        this.email = email;
        this.tel_mobile = tel_mobile;
        this.tel_fixe = tel_fixe;
        this.latitude = latitude;
        this.longitude = longitude;


    }


    public Client (String code_client, String raison_sociale, String matricule_fiscale, String registre_commerce, Double max_credit)
    {

        this.code_client = code_client;
        this.raison_sociale = raison_sociale;
        this.matricule_fiscale = matricule_fiscale;
        this.registre_commerce = registre_commerce;
        this.max_credit = max_credit;
    }



    public int getId_client() {
        return id_client;
    }


    public void setId_client(int id_client) {
        this.id_client = id_client;
    }


    public int getId() {
        return id_client;
    }

    public String getCode_client() {
        return code_client;
    }

    public String getRaison_sociale() {
        return raison_sociale;
    }

    public String getMatricule_fiscale() {
        return matricule_fiscale;
    }

    public String getRegistre_commerce() {
        return registre_commerce;
    }

    public String getEmail() {
        return email;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getTel_mobile() {
        return tel_mobile;

    }

    public String getTel_fixe() {
        return tel_fixe;
    }


    public void setId(int id) {
        this.id_client = id_client;
    }

    public void setCode_client(String code_client) {
        this.code_client = code_client;
    }

    public void setRaison_sociale(String raison_sociale) {
        this.raison_sociale = raison_sociale;
    }

    public void setMatricule_fiscale(String matricule_fiscale) {
        this.matricule_fiscale = matricule_fiscale;
    }

    public void setRegistre_commerce(String registre_commerce) {
        this.registre_commerce = registre_commerce;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel_mobile(String tel_mobile) {
        this.tel_mobile = tel_mobile;
    }

    public void setTel_fixe(String tel_fixe) {
        this.tel_fixe = tel_fixe;
    }


    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }


    public List<Client> getAllClients() {
        return clients;
    }

    public void setMovieResult(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public String toString() {
        return raison_sociale ;

    }
}
