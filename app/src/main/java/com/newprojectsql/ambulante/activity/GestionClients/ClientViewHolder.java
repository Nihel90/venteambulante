package com.newprojectsql.ambulante.activity.GestionClients;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;

public class ClientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


     ImageView img_product;
     TextView txt_raison;
     TextView txt_telMobile;

     ItemClickListener itemClickListener;


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }



    public ClientViewHolder(View itemView)
    {
        super(itemView);

        img_product = (ImageView)itemView.findViewById(R.id.image_product);
        txt_raison = (TextView)itemView.findViewById(R.id.txt_raison);
        txt_telMobile = (TextView)itemView.findViewById(R.id.txt_telMobile);

        itemView.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        itemClickListener.onClick(v);
    }
}
