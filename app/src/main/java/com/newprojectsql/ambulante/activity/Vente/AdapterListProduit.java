package com.newprojectsql.ambulante.activity.Vente;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListProduit extends BaseAdapter {

    private List<Produit> listProduit;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListProduit(List<Produit> listProduit) {
        this.listProduit = listProduit;
    }



    public AdapterListProduit(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_produits, null);

        TextView nom_produit = (TextView)vi.findViewById(R.id.nom_produit); //
        TextView prixU = (TextView)vi.findViewById(R.id.prixU); //
        TextView qte = (TextView)vi.findViewById(R.id.qte); //
        TextView prixTotal = (TextView) vi.findViewById(R.id.prixTotal);
        TextView prix_ttc = (TextView) vi.findViewById(R.id.prix_ttc);
        TextView prix_ttc_total = (TextView) vi.findViewById(R.id.prix_ttc_total);
        TextView tva = (TextView) vi.findViewById(R.id.tva);


        HashMap<String, String> produit = new HashMap<String, String>();
        produit = data.get(position);

        // Setting all values in listview
        nom_produit.setText(produit.get(Vente.KEY_NOM_PRODUIT));
        prixU.setText(produit.get(Vente.KEY_PRIX_U));
        qte.setText(produit.get(Vente.KEY_QUANTITE));
        prixTotal.setText(produit.get(Vente.KEY_PRIX_TOTAL));
        prix_ttc.setText(produit.get(Vente.KEY_PRIX_TTC));
        prix_ttc_total.setText(produit.get(Vente.KEY_PRIX_TTC_TOTAL));
        tva.setText(produit.get(Vente.KEY_TVA));

        return vi;
    }


}
