package com.newprojectsql.ambulante.activity.Chargement;

public class Depot {

    public int id_depot;
    public String nom_depot;
    public String adresse_depot;


    public Depot(int id_depot, String nom_depot, String adresse_depot) {
        this.id_depot = id_depot;
        this.nom_depot = nom_depot;
        this.adresse_depot = adresse_depot;
    }


    public int getId_depot() {
        return id_depot;
    }

    public String getNom_depot() {
        return nom_depot;
    }

    public String getAdresse_depot() {
        return adresse_depot;
    }


    public void setId_depot(int id_depot) {
        this.id_depot = id_depot;
    }

    public void setNom_depot(String nom_depot) {
        this.nom_depot = nom_depot;
    }

    public void setAdresse_depot(String adresse_depot) {
        this.adresse_depot = adresse_depot;
    }



    @Override
    public String toString() {
        return nom_depot ;

    }


}
