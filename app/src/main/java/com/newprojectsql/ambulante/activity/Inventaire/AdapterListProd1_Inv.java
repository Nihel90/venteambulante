package com.newprojectsql.ambulante.activity.Inventaire;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Chargement.Activity_Chargement;
import com.newprojectsql.ambulante.activity.Vente.Produit;

import org.w3c.dom.Text;

import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListProd1_Inv extends BaseAdapter {

    private List<Produit> listProduit;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListProd1_Inv(List<Produit> listProduit) {
        this.listProduit = listProduit;
    }



    public AdapterListProd1_Inv(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.listprod1_inventaire, null);

        TextView code_produit = (TextView)vi.findViewById(R.id.code_produit);
        TextView nom_produit = (TextView)vi.findViewById(R.id.nom_produit);
        TextView sumQte = (TextView)vi.findViewById(R.id.qte_chargee);
        TextView qteVendue = (TextView)vi.findViewById(R.id.qte_vendue);
        TextView qteRes = (TextView)vi.findViewById(R.id.qte_restante); //


        HashMap<String, String> produit = new HashMap<String, String>();
        produit = data.get(position);


        // Setting all values in listview

        code_produit.setText(produit.get(Activity_Inventaire.KEY_CODE_PRODUIT));
        nom_produit.setText(produit.get(Activity_Inventaire.KEY_NOM_PRODUIT));
        sumQte.setText(produit.get(Activity_Inventaire.KEY_SUM_Quantite));
        qteVendue.setText(produit.get(Activity_Inventaire.KEY_QUANTITE_VENDUE));
        qteRes.setText(produit.get(Activity_Inventaire.KEY_QUANTITE_RES));


        return vi;
    }







}
