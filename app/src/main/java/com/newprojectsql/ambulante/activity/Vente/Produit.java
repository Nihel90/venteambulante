package com.newprojectsql.ambulante.activity.Vente;

import java.io.Serializable;

public class Produit implements Serializable {

    public int id_produit;
    public int id_categorie;
    public String code_produit;
    public String nom_produit;
    public Double prix;
    public int tva;
    public Double prix_ttc;

    public Produit()
    {

    }


    public Produit(int id_produit, int id_categorie, String code_produit, String nom_produit, Double prix, int tva, Double prix_ttc) {
        this.id_produit = id_produit;
        this.id_categorie = id_categorie;
        this.code_produit = code_produit;
        this.nom_produit = nom_produit;
        this.prix = prix;
        this.tva = tva;
        this.prix_ttc = prix_ttc;
    }


    public int getTva() {
        return tva;
    }

    public Double getPrix_ttc() {
        return prix_ttc;
    }

    public int getId_produit() {
        return id_produit;
    }

    public int getId_categorie() {
        return id_categorie;
    }

    public String getCode_produit() {
        return code_produit;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public Double getPrix() {
        return prix;
    }


    public void setId_produit(int id_produit) {
        this.id_produit = id_produit;
    }

    public void setId_categorie(int id_categorie) {
        this.id_categorie = id_categorie;
    }

    public void setCode_produit(String code_produit) {
        this.code_produit = code_produit;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }


    public void setTva(int tva) {
        this.tva = tva;
    }

    public void setPrix_ttc(Double prix_ttc) {
        this.prix_ttc = prix_ttc;
    }

    @Override
    public String toString() {

        return nom_produit+"   " +code_produit;

    }






}
