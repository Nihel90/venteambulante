package com.newprojectsql.ambulante.activity.Reglement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.newprojectsql.ambulante.activity.Chargement.Activity_Chargement;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Vente.AdapterListProduit;
import com.newprojectsql.ambulante.activity.Vente.Facture;
import com.newprojectsql.ambulante.activity.Vente.LigneFacture;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Valider_Vente;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Reglement extends Activity implements AdapterView.OnItemSelectedListener {

    /////Declaration Api
    Api mService;

    ///Declaration des Views

    private TextView txt_num_facture;
    private TextView txt_montant_ht;
    private TextView txt_tva;
    private TextView txt_remise;
    private TextView txt_timbre;
    private TextView txt_net_payer;
    private Spinner spinner_reg;
    private EditText montant;
    private Button btn_ajout_reg;
    private ListView list;
    private TextView txt_total_reg;
    private TextView txt_reste_payer;
    private Button btn_valider;

    private TextView txt_num_reglement;
    private TextView txt_type_reglement;
    private TextView txt_montant;


    //////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    static final String KEY_NOM_PRODUIT = "nomP";
    static final String KEY_CODE_PRODUIT = "codeP";
    static final String KEY_PRIX_U = "prixU";
    static final String KEY_QUANTITE = "quantite";
    static final String KEY_PRIX_TOTAL = "prixTotal";
    static final String KEY_PRIX_TTC = "prix_ttc";
    static final String KEY_PRIX_TTC_TOTAL = "prix_ttc_total";
    static final String KEY_TVA = "tva";
    static final String KEY_NUM_REGLEMENT = "num_reglement";
    static final String KEY_TYPE_REGLEMENT = "type_reglement";
    static final String KEY_MONTANT = "montant";

    ///////////////////////////////////////////////////
    /////////////////////////////////////////////////

    ///Declaration des variables

    public Double totalReg=0.0;
    public Double reste;
    public double tva=0.0;
    public Double totalC=0.0;


    public Facture currentfacture;
    public LigneFacture currentLigneFacture;
    public Reglement currentReglement;
    public ArrayList<HashMap<String, String>> lisProd;


    AdapterListReglement adapter;
    ArrayList<HashMap<String, String>> reglementList;

    public int deleteItem;
    public String mnt;
    public double montantReg=0.0;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reglement);


        mService = RetrofitClient.getInstance().getApi();

        txt_num_facture = (TextView) findViewById(R.id.txt_num_facture);
        txt_montant_ht = (TextView) findViewById(R.id.txt_montant_ht);
        txt_tva = (TextView) findViewById(R.id.txt_tva);
        txt_remise = (TextView) findViewById(R.id.txt_remise);
        txt_net_payer = (TextView) findViewById(R.id.txt_net_payer);
        txt_timbre = (TextView)findViewById(R.id.txt_timbre);

        spinner_reg = (Spinner) findViewById(R.id.spinnerR);

        montant = (EditText) findViewById(R.id.montant);

        btn_ajout_reg = (Button) findViewById(R.id.btn_ajout_reg);
        btn_valider = (Button)findViewById(R.id.btn_valider);

        list= (ListView)findViewById(R.id.list);
        txt_total_reg = (TextView) findViewById(R.id.txt_total_reg) ;
        txt_reste_payer = (TextView) findViewById(R.id.txt_reste_payer);


        txt_num_reglement = (TextView) findViewById(R.id.txt_num_reglement);
        txt_type_reglement = (TextView) findViewById(R.id.txt_type_reglement);
        txt_montant = (TextView) findViewById(R.id.txt_montant);


        currentfacture = (Facture) getIntent().getSerializableExtra("FACTURE");
        lisProd =  (ArrayList<HashMap<String, String>>)getIntent().getSerializableExtra("LIGNESFACTURE");

        currentLigneFacture = new LigneFacture();
        currentReglement = new Reglement();



        ////////////////////////get id_facture ////////////////////////



        Call<Integer> call = RetrofitClient
                .getInstance()
                .getApi()
                .GetIDFacture(currentfacture.num_facture);

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                int s;
                s= response.body();

                currentfacture.id_facture = s;

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });


        //////////////Détails Facture /////////////

        txt_num_facture.setText(currentfacture.num_facture);
        txt_montant_ht.setText(currentfacture.prix_ht.toString());

        tva = Get_TVA();
        tva=  Math.round(tva*1000.0)/1000.0;
        txt_tva.setText(Double.valueOf(tva).toString());
        txt_remise.setText(currentfacture.remise.toString());
        txt_timbre.setText(currentfacture.timbre_f.toString());
        txt_net_payer.setText(currentfacture.prix_ttc_total.toString());

        txt_total_reg.setText(totalReg.toString());
        reste = currentfacture.prix_ttc_total;
        txt_reste_payer.setText(reste.toString());


        /////Initialiser Spinner Type de règlement:


        // Spinner Drop down elements
        final List<String> categories = new ArrayList<String>();
      //  categories.add("");
        categories.add("Espèce");
        categories.add("Chèque");
        categories.add("Traite");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter <String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_reg.setAdapter(dataAdapter);


        spinner_reg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item

                String pos= categories.get(position);
                currentReglement.type_reglement= pos;

                // On selecting a spinner item

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        //////////////////////Code for list view////////////////////////////////
        ///////////////////////////////////////////////////////////////////////
        reglementList = new ArrayList<HashMap<String, String>>();
        adapter = new AdapterListReglement(Activity_Reglement.this, reglementList);




        ///////////onClick de button ajout_reglement////////////
        btn_ajout_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Insert_Data_inList();
                Clear_Montant();

            }
        });


        //////////onClick de button valider////////////////////
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(Activity_Reglement.this)
                        .setTitle("Valider Règlement")
                        .setMessage("Etes vous sûr de vouloir valider ce(s) règlement(s) ?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                try {
                                    UpdateVals();
                                    AddReglement();
                                    Update_Facture();


                                    if (currentfacture.credit==0)
                                    {
                                        Update_Paid_Facture();
                                    }

                                    Intent activity = new Intent(Activity_Reglement.this, MainActivity.class);
                                    startActivity(activity);
                                    finish();

                                }

                                catch (Exception Ex)
                                {
                                    Toast.makeText(Activity_Reglement.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }).setNegativeButton("Non", null).show();

            }

        });




}




    public void Update_Paid_Facture()

   {
       Call<ResponseBody> call = RetrofitClient
               .getInstance()
               .getApi()
               .UpdatePaidFacture(currentfacture.id_facture);


       call.enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

               String s = null;

               try {

                   s = response.body().string();
                   String h = s.replace("\"", "");

             //      Toast.makeText(Activity_Reglement.this, s, Toast.LENGTH_SHORT).show();

               } catch (IOException e) {
                   e.printStackTrace();
               }

           }


           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {

               Toast.makeText(Activity_Reglement.this, t.getMessage(), Toast.LENGTH_LONG).show();

           }
       });



   }






   public void Update_Facture()
   {

       Call<ResponseBody> call = RetrofitClient
               .getInstance()
               .getApi()
               .updateFacture(currentfacture.total_reglement, currentfacture.credit, currentfacture.id_facture);


       call.enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

               String s = null;

               try {

                   s = response.body().string();
                   String h = s.replace("\"", "");

              //     Toast.makeText(Activity_Reglement.this, s, Toast.LENGTH_SHORT).show();

                   Update_Client();
               } catch (IOException e) {
                   e.printStackTrace();
               }


           }


           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {

               Toast.makeText(Activity_Reglement.this, t.getMessage(), Toast.LENGTH_LONG).show();

           }
       });



    }





    public void Clear_Montant()
    {
        montant.setText("");
    }



    public void  AddReglement() {

        for (int i = 0; i < reglementList.size(); i++) {
            currentReglement.type_reglement = reglementList.get(i).get(KEY_TYPE_REGLEMENT);
            currentReglement.montant = Double.valueOf(reglementList.get(i).get(KEY_MONTANT));


            /*Do add Lignefacture using the api call*/
            Call<ResponseBody> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .createReglement(  currentReglement.type_reglement, currentReglement.montant, currentfacture.id_facture);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    String s = null;

                    try {

                        s = response.body().string();
                       // String h = s.replace("\"", "");

                        Toast.makeText(Activity_Reglement.this, "" + s, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        }
    }









    public void Insert_Data_inList() {

        mnt = montant.getText().toString().trim();
        UpdateVals();

        try {

            if (mnt.length() > 0) {
                montantReg = Double.valueOf(mnt);
                montantReg= Math.round(montantReg*1000.0)/1000.0;

                if ((montantReg > 0)&& (montantReg<= currentfacture.credit)){

                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put(KEY_NUM_REGLEMENT, reglementList.size()+1 +"");
                    map.put(KEY_TYPE_REGLEMENT, currentReglement.type_reglement);
                    map.put(KEY_MONTANT, montantReg + "");

                    reglementList.add(map);
                    list.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                    list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                        @Override
                        public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                       int pos, long arg3) {
                            deleteItem = pos;
                            // Creating a new alert dialog to confirm the delete
                            AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                                    .setTitle("Etes vous sûr de vouloir supprimer ce règlement")
                                    .setPositiveButton("Oui",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int whichButton) {

                                                    try {
                                                        reglementList.remove(deleteItem);

                                                        adapter.notifyDataSetChanged();
                                                        dialog.dismiss();
                                                        UpdateVals();

                                                    } catch (Exception e) {
                                                        Toast.makeText(Activity_Reglement.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            })
                                    .setNegativeButton("Non",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int whichButton) {
                                                    // When you press cancel, just close the
                                                    // dialog
                                                    dialog.cancel();
                                                }
                                            }).show();

                            return false;

                        }
                    });

                    UpdateVals();


                } else {

                 //   Toast.makeText(this, "Le reste à payer est " +currentfacture.credit, Toast.LENGTH_SHORT).show();

                    final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Reglement.this).create();
                    alertDialog.setTitle("Erreur");
                    alertDialog.setMessage("Le reste à payer est" +currentfacture.credit);
                    alertDialog.setIcon(R.drawable.error);
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.cancel();
                        } });
                    alertDialog.show();
                }
            } else {
               // Toast.makeText(this, "Veuillez insérer le montant", Toast.LENGTH_SHORT).show();

                final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Reglement.this).create();
                alertDialog.setTitle("Erreur");
                alertDialog.setMessage("Veuillez insérer le montant");
                alertDialog.setIcon(R.drawable.error);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.cancel();
                    } });
                alertDialog.show();


            }

        }
        catch (Exception Ex) {
            Toast.makeText(this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }






    public Double Get_TVA()
    {
        Double val_tva=0.0;
        Double pUnitaire=0.0;
        int t;

        for (int i=0; i<lisProd.size(); i++)
        {

            currentLigneFacture.prix_unitaire = Double.valueOf(lisProd.get(i).get(KEY_PRIX_U));
            currentLigneFacture.tva = Integer.parseInt(lisProd.get(i).get(KEY_TVA));


            currentLigneFacture.prix_ttc_total = Double.valueOf(lisProd.get(i).get(KEY_PRIX_TTC_TOTAL));
            currentLigneFacture.prix_total = Double.valueOf(lisProd.get(i).get(KEY_PRIX_TOTAL));






            pUnitaire = currentLigneFacture.prix_unitaire;
            t= currentLigneFacture.tva;

            val_tva = val_tva + (pUnitaire*t)/100;
        }

        return val_tva;

    }





    public void UpdateVals()
    {

        Double tot=0.0;
        Double res=currentfacture.prix_ttc_total;

        for (int i=0; i<reglementList.size(); i++)
        {
            montantReg=Double.valueOf(reglementList.get(i).get(KEY_MONTANT));
            tot=tot+montantReg;
            tot= Math.round(tot*1000.0)/1000.0;

            res = currentfacture.prix_ttc_total - tot;
            res= Math.round(res*1000.0)/1000.0;
        }

        txt_total_reg.setText(tot.toString());
        txt_reste_payer.setText(res.toString());
        currentfacture.total_reglement = tot;
        currentfacture.credit = res;

    }




    public void Update_Client()
    {

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .updateClient(currentfacture.id_client);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                String s = null;

                try {

                    s = response.body().string();
                    // String h = s.replace("\"", "");

                 //   Toast.makeText(Activity_Reglement.this, "" + s, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });





    }





    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }


}
