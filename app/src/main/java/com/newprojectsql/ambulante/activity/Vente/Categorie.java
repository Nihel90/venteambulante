package com.newprojectsql.ambulante.activity.Vente;

import java.io.Serializable;

public class Categorie  implements Serializable {

    public int id_categorie;
    public String designation;

    public Categorie()
    {

    }


    public Categorie(int id_categorie, String designation) {
        this.id_categorie = id_categorie;
        this.designation = designation;
    }


    public int getId_categorie() {
        return id_categorie;
    }

    public String getDesignation() {
        return designation;
    }

    public void setId_categorie(int id_categorie) {
        this.id_categorie = id_categorie;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public String toString() {
        return designation ;

    }


}
