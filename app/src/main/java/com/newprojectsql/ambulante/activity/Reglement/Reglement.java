package com.newprojectsql.ambulante.activity.Reglement;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import java.io.Serializable;
import java.util.HashMap;


public class Reglement implements Serializable {

public int  id_reglement;
public int id_facture;
public String type_reglement;
public Double montant;


    public Reglement()
    {

    }


    public void setId_reglement(int id_reglement) {
        this.id_reglement = id_reglement;
    }

    public void setId_facture(int id_facture) {
        this.id_facture = id_facture;
    }

    public void setType_reglement(String type_reglement) {
        this.type_reglement = type_reglement;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public int getId_reglement() {
        return id_reglement;
    }

    public int getId_facture() {
        return id_facture;
    }

    public String getType_reglement() {
        return type_reglement;
    }

    public Double getMontant() {
        return montant;
    }
}
