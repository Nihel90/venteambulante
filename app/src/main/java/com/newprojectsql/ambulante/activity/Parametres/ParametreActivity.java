package com.newprojectsql.ambulante.activity.Parametres;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Chargement.Depot;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParametreActivity extends AppCompatActivity {


    /////Declaration Api

    Api mService;
    Context context;

    /////Declaration des listes

    private List<Depot> listeDepot;


    /////Déclaration Spinner

    private Spinner spinnerDepot;

    public Depot currentDepot = null;
    int idDepot;



    ////// Shared Preferences ////////////

    private TextView textView1, textView2;

    private Button saveButton;


    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";
    public static final String SWITCH1 = "switch1";

    private String text;
    private boolean switchOnOff;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametre);


        mService = RetrofitClient.getInstance().getApi();

        listeDepot = new ArrayList<>();


        // Spinner element
        spinnerDepot = (Spinner) findViewById(R.id.spinnerCategorie);

        ////textView and buttons
        textView1 = (TextView) findViewById(R.id.textview1);
        textView2 = (TextView) findViewById(R.id.textview2);

        saveButton = (Button) findViewById(R.id.save_button);



        /////Appeler la méthode getListDepot pour remplir le spinner avec liste des dépots
        listeDepot = getListDepot();

        //////////////////spinner Depot///////////////////////////////////////////////

        spinnerDepot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentDepot = listeDepot.get(position);
                idDepot = currentDepot.id_depot;

                textView1.setText(idDepot+"");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
                loadData();
                updateViews();

            }
        });

        loadData();
        updateViews();


    }





    /////////////////Retourner liste des dépots////////////////////////////////

    private List<Depot> getListDepot()
    {

        try
        {
            mService.getListDepot().enqueue(new Callback<List<Depot>>() {
                @Override
                public void onResponse(Call<List<Depot>> call, Response<List<Depot>> response) {

                    listeDepot= response.body();
                    ArrayAdapter<Depot> dataAdapterDepots = new ArrayAdapter<Depot>(ParametreActivity.this, android.R.layout.simple_spinner_item, listeDepot);
                    dataAdapterDepots.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDepot.setAdapter(dataAdapterDepots);
                }

                @Override
                public void onFailure(Call<List<Depot>> call, Throwable t) {

                }
            });
        }

        catch (Exception Ex)
        {
            Toast.makeText(context, Ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return listeDepot;
    }



    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TEXT, textView1.getText().toString());

        editor.apply();

        Toast.makeText(this, "Donnée sauvegardée avec succès", Toast.LENGTH_SHORT).show();
    }

    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");
        switchOnOff = sharedPreferences.getBoolean(SWITCH1, false);
    }


    public void updateViews() {
        textView2.setText(text);
        spinnerDepot.setSelection(0);

    }







}
