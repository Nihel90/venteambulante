package com.newprojectsql.ambulante.activity.GestionClients;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.newprojectsql.ambulante.BuildConfig;
import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static com.newprojectsql.ambulante.activity.GestionClients.Fiche_client.currentClient;

public class Ajouter_client extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private final AppCompatActivity activity = Ajouter_client.this;



   /////////Déclaration des variables pour détecter la localisation du client/////////
    //////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = Ajouter_client.class.getSimpleName();

    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;


    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;


 //   private Double latitude, longitude;


    ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////



    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutCodeClient;
    private TextInputLayout textInputLayoutRaisonSociale;
    private TextInputLayout textInputLayoutMatriculeFiscale;
    private TextInputLayout textInputLayoutRegistreCommerce;
    private TextInputLayout textInputLayoutAdresseMail;
    private TextInputLayout textInputLayoutNumTelMobile;
    private TextInputLayout textInputLayoutNumTelFixe;
    private TextInputLayout textInputLayoutMaxCredit;


    private TextInputEditText textInputEditTextCodeClient;
    private TextInputEditText textInputEditTextRaisonSociale;
    private TextInputEditText textInputEditTextMatriculeFiscale;
    private TextInputEditText textInputEditTextRegistreCommerce;
    private TextInputEditText textInputEditTextAdresseMail;
    private TextInputEditText textInputEditTextNumTelMobile;
    private TextInputEditText textInputEditTextNumTelFixe;
    private TextInputEditText textInputEditTextMaxCredit;


    private AppCompatButton appCompatButtonRegister;


    public Client client;
    public String code_client, raison_sociale, matricule_fiscale, registre_commerce, email, tel_mobile, tel_fixe;
    public String max_credit;

    public int nombreClients;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_client);



        textInputLayoutCodeClient = (TextInputLayout) findViewById(R.id.textInputLayoutCodeClient);
        textInputLayoutRaisonSociale = (TextInputLayout) findViewById(R.id.textInputLayoutRaisonSociale);
        textInputLayoutMatriculeFiscale = (TextInputLayout) findViewById(R.id.textInputLayoutMatriculeFiscale);
        textInputLayoutRegistreCommerce = (TextInputLayout) findViewById(R.id.textInputLayoutRegistreCommerce);
        textInputLayoutAdresseMail = (TextInputLayout) findViewById(R.id.textInputLayoutAdresseMail);
        textInputLayoutNumTelMobile = (TextInputLayout) findViewById(R.id.textInputLayoutNumTelMobile);
        textInputLayoutNumTelFixe = (TextInputLayout) findViewById(R.id.textInputLayoutNumTelFixe);
        textInputLayoutMaxCredit = (TextInputLayout) findViewById(R.id.textInputLayoutMaxCredit);


        textInputEditTextCodeClient = (TextInputEditText) findViewById(R.id.textInputEditTextCodeClient);
        textInputEditTextRaisonSociale = (TextInputEditText) findViewById(R.id.textInputEditTextRaisonSociale);
        textInputEditTextMatriculeFiscale = (TextInputEditText) findViewById(R.id.textInputEditTextMatriculeFiscale);
        textInputEditTextRegistreCommerce = (TextInputEditText) findViewById(R.id.textInputEditTextRegistreCommerce);
        textInputEditTextAdresseMail = (TextInputEditText) findViewById(R.id.textInputEditTextAdresseMail);
        textInputEditTextNumTelMobile = (TextInputEditText) findViewById(R.id.textInputEditTextNumTelMobile);
        textInputEditTextNumTelFixe = (TextInputEditText) findViewById(R.id.textInputEditTextNumTelFixe);
        textInputEditTextMaxCredit = (TextInputEditText) findViewById(R.id.textInputEditTextMaxCredit);



     //   latitude= 3.88;
     //   longitude =3.256;


        ///////////////////////////////////////////////////////
        //////////////////////////////location ////////////////

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        init();
        startLoc();



        ////////////////////////get nombre des clients ////////////////////////
        Call<Integer> call = RetrofitClient
                .getInstance()
                .getApi()
                .getNombreClients();

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                int s;

                s = response.body();
                //  String h = s.replace("\"", "");

                nombreClients = s;
                nombreClients = nombreClients + 1;
                code_client = "Client" + "_00" + nombreClients;

                textInputEditTextCodeClient.setText(code_client);

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

                Toast.makeText(Ajouter_client.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });
        ///////////////////////////////////////////////////////////////////////////////////////


        // Create button
        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);

        appCompatButtonRegister.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                AjoutClient();
            }
        });

    }


    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());


            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }



    /* Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "Tous les paramètres de localisation sont satisfaits.");

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Les paramètres d'emplacement ne sont pas satisfaits. Tenter de mettre à niveau " +
                                        "les paramètres de localisation\n ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(Ajouter_client.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent est incapable d'exécuter la demande.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Les paramètres de localisation sont inadéquats et ne peuvent pas être " +
                                        "\"corrigé ici. Fixé dans Paramètres.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(Ajouter_client.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }





    public void startLoc()
    {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }



    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //    Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }

    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }



    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspendue");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.i(TAG, "Echec de connexion. Erreur: " + connectionResult.getErrorCode());
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }










    private void AjoutClient()
    {


        ////////////////////////get nombre des clients ////////////////////////
        Call<Integer> call = RetrofitClient
                .getInstance()
                .getApi()
                .getNombreClients();

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                int s;

                s = response.body();
                //  String h = s.replace("\"", "");

                nombreClients = s;
                nombreClients = nombreClients + 1;
                code_client = "Client" + "_00" + nombreClients;

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

                Toast.makeText(Ajouter_client.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });


       code_client= textInputEditTextCodeClient.getText().toString().trim();
       raison_sociale= textInputEditTextRaisonSociale.getText().toString().trim();
       matricule_fiscale= textInputEditTextMatriculeFiscale.getText().toString().trim();
       registre_commerce= textInputEditTextRegistreCommerce.getText().toString().trim();
       email= textInputEditTextAdresseMail.getText().toString().trim();
       tel_mobile= textInputEditTextNumTelMobile.getText().toString().trim();
       tel_fixe= textInputEditTextNumTelFixe.getText().toString().trim();
       max_credit = textInputEditTextMaxCredit.getText().toString().trim();

       try {

         /*  if (code_client.isEmpty()) {
               textInputEditTextCodeClient.setError("Le champs 'Code Client' est obligatoire");
               textInputEditTextCodeClient.requestFocus();
               return;
           }
         */

           if (raison_sociale.isEmpty()) {
               textInputEditTextRaisonSociale.setError("Le champ 'Raison Sociale' est obligatoire");
               textInputEditTextRaisonSociale.requestFocus();
               return;
           }

           if (matricule_fiscale.isEmpty()) {
               textInputEditTextMatriculeFiscale.setError("Le champ 'Matricule Fiscale' est obligatoire");
               textInputEditTextMatriculeFiscale.requestFocus();
               return;
           }

           if (registre_commerce.isEmpty()) {
               textInputEditTextRegistreCommerce.setError("Le champ 'Registre de Commerce' est obligatoire");
               textInputEditTextRegistreCommerce.requestFocus();
               return;
           }


     /*      if (email.isEmpty()) {
               textInputEditTextAdresseMail.setError("Le champs 'Adresse Mail' est obligatoire");
               textInputEditTextAdresseMail.requestFocus();
               return;
           }  */

           if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()  && (  ! email.isEmpty())) {
               textInputEditTextAdresseMail.setError("Veuillez entrer une adresse mail valide");
               textInputEditTextAdresseMail.requestFocus();
               return;
           }


           if (tel_mobile.isEmpty()) {
               textInputEditTextNumTelMobile.setError("Le champ 'telephone Mobile' est obligatoire");
               textInputEditTextNumTelMobile.requestFocus();
               return;
           }


           if (tel_mobile.length() < 8) {
               textInputEditTextNumTelMobile.setError("Veuillez insérer un numéro de téléphone mobile valide");
               textInputEditTextNumTelMobile.requestFocus();
               return;
           }


       /*    if (tel_fixe.isEmpty()) {
               textInputEditTextNumTelFixe.setError("Le champs 'telephone Fixe' est obligatoire");
               textInputEditTextNumTelFixe.requestFocus();
               return;
           }*/

          if ((tel_fixe.length() < 8) && (  ! tel_fixe.isEmpty()  ) ) {
               textInputEditTextNumTelFixe.setError("Veuillez insérer un numéro de téléphone fixe valide");
               textInputEditTextNumTelFixe.requestFocus();
               return;
           }



          if (max_credit.isEmpty()){
               textInputEditTextNumTelFixe.setError("Le champs 'Max Credit' est obligatoire");
               textInputEditTextNumTelFixe.requestFocus();
               return;
           }


           double latitude= mCurrentLocation.getLatitude();
           double longitude =mCurrentLocation.getLongitude();


         /////pass a client in intent


               Intent activity = new Intent(Ajouter_client.this, Ajouter_adresse.class);


                //To pass:
                // in Activity A, when starting Activity B

                client = new Client(code_client, raison_sociale,
                        matricule_fiscale, registre_commerce, email,
                        tel_mobile, tel_fixe, latitude, longitude, Double.valueOf(max_credit));


                 activity.putExtra("CLIENT", client);
                 Ajouter_client.this.startActivity(activity);

                }

                catch(Exception ex)

                {
                   Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
               }


              }


    @Override
    public void onClick(View v) {

    }







}
