package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.DownloadManager;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.newprojectsql.ambulante.activity.Chargement.Depot;
import com.newprojectsql.ambulante.activity.Vente.Categorie;
import com.newprojectsql.ambulante.activity.Vente.Facture;
import com.newprojectsql.ambulante.activity.Vente.Produit;

import java.util.Date;
import java.util.List;

import okhttp3.Address;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {


    @FormUrlEncoded
    @POST("CreateClient.php")
    Call<ResponseBody> CreateClient(

            @Field("code_client") String code_client,
            @Field("raison_sociale") String raison_sociale,
            @Field("matricule_fiscale") String matricule_fiscale,
            @Field("registre_commerce") String registre_commerce,
            @Field("email") String email,
            @Field("tel_mobile") String tel_mobile,
            @Field("tel_fixe") String tel_fixe,
            @Field("latitude") Double latitude,
            @Field("longitude") Double longitude,
            @Field("max_credit") Double max_credit

    );




    @FormUrlEncoded
    @POST("CreateAddressFac.php")
    Call<ResponseBody> CreateAddressFac(

            @Field("adresse") String code_client,
            @Field("gouvernorat") String raison_sociale,
            @Field("ville") String registre_commerce,
            @Field("id_client") Integer id_client

    );


    @FormUrlEncoded
    @POST("CreateAddressLiv.php")
    Call<ResponseBody> CreateAddressLiv(

            @Field("adresse") String code_client,
            @Field("gouvernorat") String raison_sociale,
            @Field("ville") String registre_commerce,
            @Field("id_client") Integer id_client

    );




    @GET("AllClients.php")
    Call<List<Client>> getListeClt();



   @GET("AllAddress.php")
    Call<List<Adresse>> getListAdr(@Query("id_client") int id_client);



    @GET("getAddressLiv.php")
    Call<List<Adresse>> getAddressLiv(@Query("id_client") int id_client);


    @GET("getAddressFac.php")
    Call<List<Adresse>> getAddressFac(@Query("id_client") int id_client);



   @GET("AllCategories.php")
    Call<List<Categorie>> getListCategories();


   @GET("AllProduitByCategorie.php")
    Call<List<Produit>> getListProduit(@Query("id_categorie") int id_categorie);



    @FormUrlEncoded
    @POST("createFacture.php")
    Call<ResponseBody> createFacture(

            @Field("num_facture") String num_facture,
            @Field("id_client") Integer id_client,
            @Field("raison_sociale") String raison_sociale,
            @Field("prix_ht") Double prix_ht,
            @Field("prix_ttc") Double prix_ttc,
            @Field("prix_ttc_total") Double prix_ttc_total,
            @Field("adresse_fac") String adresse_fac,
            @Field("adresse_liv") String adresse_liv,
            @Field("remise") Double remise,
            @Field("timbre_fiscale") Double timbre_fiscale

    );



    @FormUrlEncoded
    @POST("createLigneFacture.php")
    Call<ResponseBody> createLigneFacture(

            @Field("code_produit") String code_produit,
            @Field("nom_produit") String nom_produit,
            @Field("prix_total") Double prix_total,
            @Field("prix_unitaire") Double prix_unitaire,
            @Field("qte") Integer qte,
            @Field("id_facture") Integer id_facture,
            @Field("prix_ttc") Double prix_ttc,
            @Field("prix_ttc_total") Double prix_ttc_total,
            @Field("tva") Integer tva,
            @Field("id_depot") int id_depot

    );



    @GET("getNombreFacture.php")
   Call<Integer> getNbFacture();



    @GET("getTimbre.php")
    Call<Double> getTimbre();


    @FormUrlEncoded
    @POST("updateFacture.php")
    Call<ResponseBody> updateFacture (

            @Field("total_reglement") Double total_reglement,
            @Field("credit") Double credit,
            @Field("id_facture") int id_facture

    );


    @FormUrlEncoded
    @POST("UpdatePaidFacture.php")
    Call<ResponseBody> UpdatePaidFacture(

            @Field("id_facture") int id_facture
    );




    @FormUrlEncoded
    @POST("updateClient.php")
    Call<ResponseBody> updateClient(

            @Field ("id_client") int id_client

    );


    @FormUrlEncoded
    @POST("createReglement.php")
    Call<ResponseBody> createReglement(

            @Field("type_reglement") String type_reglement,
            @Field("montant") Double montant,
            @Field("id_facture") int id_facture

    );


    @GET("GetIDFacture.php")
    Call<Integer> GetIDFacture(@Query("num_facture") String num_facture);



    @GET("getTotalCredit.php")
    Call<Double> getTotalCredit(@Query("id_client") int id_client);



    @GET("getFacturesNonR.php")
    Call<List<Facture>> getFacturesNonR(@Query("id_client") int id_client);



    @GET("getMax_Credit.php")
    Call<Double> getMax_Credit(@Query("id_client") int id_client);



    @FormUrlEncoded
    @POST("createChargement.php")
    Call<ResponseBody> createChargement(

            @Field("num_chargement") String num_chargement,
            @Field("id_depot") int id_depot

    );



    @FormUrlEncoded
    @POST("createDetailsChargement.php")
    Call<ResponseBody> createDetailsChargement(

            @Field("id_depot") int id_depot,
            @Field("code_produit") String code_produit,
            @Field("quantite") int quantite,
            @Field("id_chargement") int id_chargement

    );



    @GET("getListDepot.php")
    Call<List<Depot>> getListDepot();



    @GET("getNbChargement.php")
    Call<Integer> getNbChargement();



    @GET("getQteStock.php")
    Call<ResponseBody> getQteStock(@Query("code_produit") String code_produit,
                              @Query("id_depot") int id_depot);



    @GET("getNombreClients.php")
    Call<Integer> getNombreClients();




    @GET("getCurrentDate.php")
    Call<ResponseBody> getCurrentDate();



    @GET("getCodeOnDate.php")
    Call<List<Produit>> getCodeOnDate(@Query("date_chargement") String date_chargement,
                                     @Query("id_depot") int id_depot);




    @GET("getQuantiteRestanteOnDate.php")
        Call<Integer> getQuantiteRestanteOnDate( @Query("code_produit") String code_produit,
                                     @Query("date_chargement") String date_chargement,
                                     @Query("id_depot") int id_depot);





    @GET("getSumQuantiteOnDate.php")
    Call<Integer> getSumQuantiteOnDate( @Query("code_produit") String code_produit,
                                             @Query("date_chargement") String date_chargement,
                                             @Query("id_depot") int id_depot);




    @GET("getQteVendueOnDate.php")
    Call<Integer> getQteVendueOnDate( @Query("code_produit") String code_produit,
                                        @Query("date") String date);



    @FormUrlEncoded
    @POST("updateStock.php")
    Call<ResponseBody> updateStock(

            @Field ("quantite") int quantite,
            @Field ("code_produit") String code_produit,
            @Field("id_depot") int id_depot
    );


    @FormUrlEncoded
    @POST("createInventaire.php")
    Call<ResponseBody> createInventaire(

            @Field("date_inventaire") String date_inventaire,
            @Field("num_inventaire") String  num_inventaire,
            @Field("id_depot") int id_depot

    );


    @FormUrlEncoded
    @POST("createLigneInventaire.php")
    Call<ResponseBody> createLigneInventaire(

            @Field("nom_produit") String nom_produit,
            @Field("code_produit") String  code_produit,
            @Field("qte_chargee") int qte_chargee,
            @Field("qte_vendue") int qte_vendue,
            @Field("qte_restante") int qte_restante,
            @Field("qte_inv") int qte_inv,
            @Field("ecart") int ecart,
            @Field("id_inventaire") int id_inventaire

    );


    @GET("getNombreInventaire.php")
    Call<Integer> getNombreInventaire();



    @FormUrlEncoded
    @POST("isInventaireExist.php")
    Call<Boolean> isInventaireExist(


         @Field("id_depot") int id_depot,
         @Field("date_inventaire") String date_inventaire);



    @FormUrlEncoded
    @POST("updateLigneInventaire.php")
    Call<ResponseBody> updateLigneInventaire(

         @Field ("qte_inv") int qte_inv,
         @Field ("ecart") int ecart,
         @Field ("code_produit") String code_produit,
         @Field ("date_inventaire") String date_inventaire,
         @Field ("id_depot") int id_depot,

         @Field ("qte_chargee") int qte_chargee,
         @Field ("qte_vendue") int qte_vendue,
         @Field ("qte_restante") int qte_restante,
         @Field ("nom_produit") String nom_produit);




    @GET("getPasswordParametre.php")
    Call<ResponseBody> getPasswordParametre();




}