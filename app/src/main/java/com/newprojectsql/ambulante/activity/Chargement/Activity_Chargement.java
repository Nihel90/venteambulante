package com.newprojectsql.ambulante.activity.Chargement;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Vente.Categorie;
import com.newprojectsql.ambulante.activity.Vente.Produit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.SHARED_PREFS;
import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.SWITCH1;
import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.TEXT;


public class Activity_Chargement extends Activity implements AdapterView.OnItemSelectedListener{


    /////Declaration Api
    Api mService;
    Context context;

    /////Declaration des listes

    private List<Depot> listeDepot;
    private List<Categorie> listeCategories;
    private List<Produit> listeProduits;


    private Spinner spinnerDepot, spinnerCategorie, spinnerNomP;
    private Button btn_valider, btn_suivant;

    private EditText qte;


    public String item;
    public String qteProduit;
    public int qteP;

    public int nombre_chargement;
    public String num_chargement;



    public  Categorie currentCategorie = null;
    public Produit currentProduit = null;
    public Depot currentDepot = null;

    private BonChargement currentChargement= null;
    public DetailsChargement currentDetailsChargement = null;
    public Stock stock;

    int idCategorie, idProduit, idDepot;

    // contains the id of the item we are about to delete
    public int deleteItem;

    public   int testExist=-1;

////////////////code pour list//////////////////
//////////////////////////////////////////////
///////////////////////////////////////////////////////
    static final String KEY_CATEGORIE = "categorie";
    static final String KEY_PRODUIT = "produit";
    static final String KEY_QUANTITE = "quantite";
    static final String KEY_CODE_PRODUIT = "code_produit";

    private ListView list;

    private Produit produit;

    private TextView categorie;
    private TextView nom_prod;
    private TextView quantite;


    AdapterListProd_Chargement adapter;
    ArrayList<HashMap<String, String>> produitList;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargement);


         /////////////////// désactiver le spinner de dépot ////////// (pour le cas d'un magasinier/////////////////



        //////////////////SharedPreferences  get idDep : donnée sauvegardée dans "Paramètres"////////////////////////

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String p = sharedPreferences.getString(TEXT, "");

        idDepot = Integer.valueOf(p);



         //  Toast.makeText(this, ""+idDepot, Toast.LENGTH_SHORT).show();
        // ///////////////////////////////////////////////////////////////////////////////////////////////////////////



        mService = RetrofitClient.getInstance().getApi();

        listeCategories = new ArrayList<>();
        listeProduits = new ArrayList<>();
        listeDepot = new ArrayList<>();



        // Spinner element
        spinnerDepot = (Spinner) findViewById(R.id.spinnerDepot);
        spinnerCategorie = (Spinner) findViewById(R.id.spinnerCategorie);
        spinnerNomP = (Spinner) findViewById(R.id.spinnerNomP);



        qte = (EditText) findViewById(R.id.quantite);
        btn_valider = (Button) findViewById(R.id.btn_valider);
        btn_suivant = (Button) findViewById(R.id.btn_suivant);



        /////////////////////////list/////////
        //////////////////////////////////////////////////////
        produitList = new ArrayList<HashMap<String, String>>();
        adapter = new AdapterListProd_Chargement(Activity_Chargement.this, produitList);

        categorie = (TextView) findViewById(R.id.categorie);
        nom_prod = (TextView) findViewById(R.id.produit);
        quantite = (TextView) findViewById(R.id.qte);

        produit = new Produit();
        list = (ListView) findViewById(R.id.list);


        currentChargement = new BonChargement();
        currentDetailsChargement = new DetailsChargement();
        stock = new Stock();


        spinnerDepot.setEnabled(false);


        ////////////////////////get nbombre des chargements ////////////////////////
        Call<Integer> call = RetrofitClient
                .getInstance()
                .getApi()
                .getNbChargement();

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);

                int s;

                s = response.body();

                nombre_chargement = s;
                nombre_chargement = nombre_chargement + 1;
                num_chargement = "Chargement " + year + "_" + nombre_chargement;

            }



            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

                Toast.makeText(Activity_Chargement.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });

        ///////////////////////////////////////////////////////////////////////////////////


        //////////action sur bouton "valider" ///////////

        btn_valider.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                InsertDataInList();
                Clear_Quantite();

            }
        });




        //////////action sur bouton "suivant" //////////

       btn_suivant.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {


               if (produitList.size() != 0) {

                   new AlertDialog.Builder(Activity_Chargement.this)
                           .setTitle("Valider Chargement")
                           .setMessage("Etes vous sûr de vouloir valider ce chargement ?")
                           .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {

                                   ////ajouter bon de chargement/////
                                   Add_bon_chargement();

                               }
                           }).setNegativeButton("Non", null).show();


               }

               else
               {

                   final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Chargement.this).create();
                   alertDialog.setTitle("Erreur");
                   alertDialog.setMessage("Veuillez insérer les produits");
                   alertDialog.setIcon(R.drawable.error);
                   alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int which) {
                           alertDialog.cancel();
                       } });
                   alertDialog.show();

               }


           }
       });



        listeDepot = getListDepot();
        listeCategories = getListCategories();



        //////////////////spinner Depot///////////////////////////////////////////////

        spinnerDepot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentDepot = listeDepot.get(position);
            //    idDepot = currentDepot.id_depot;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


         ////////////////spinner categorie////////
        spinnerCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentCategorie = listeCategories.get(position);
                idCategorie = currentCategorie.id_categorie;
                listeProduits = getListProduit(idCategorie);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        ////////////////spinner produits//////////////
        spinnerNomP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentProduit = listeProduits.get(position);
                idProduit = currentProduit.id_produit;

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }




    /////////////////Retourner liste des dépots////////////////////////////////

    private List<Depot> getListDepot()
    {

        try
        {
            mService.getListDepot().enqueue(new Callback<List<Depot>>() {
                @Override
                public void onResponse(Call<List<Depot>> call, Response<List<Depot>> response) {

                    listeDepot= response.body();
                    ArrayAdapter<Depot> dataAdapterDepots = new ArrayAdapter<Depot>(Activity_Chargement.this, android.R.layout.simple_spinner_item, listeDepot);
                    dataAdapterDepots.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDepot.setAdapter(dataAdapterDepots);
                }

                @Override
                public void onFailure(Call<List<Depot>> call, Throwable t) {

                }
            });
        }

        catch (Exception Ex)
        {
            Toast.makeText(context, Ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return listeDepot;
    }




    /////////////////Retourner liste des catégories////////////////////////////

    private List<Categorie> getListCategories() {
        try {
            mService.getListCategories().enqueue(new Callback<List<Categorie>>() {
                @Override
                public void onResponse(Call<List<Categorie>> call, Response<List<Categorie>> response) {

                    listeCategories = response.body();
                    ArrayAdapter<Categorie> dataAdapterCategories = new ArrayAdapter<Categorie>(Activity_Chargement.this, android.R.layout.simple_spinner_item, listeCategories);
                    dataAdapterCategories.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerCategorie.setAdapter(dataAdapterCategories);

                }

                @Override
                public void onFailure(Call<List<Categorie>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception ex) {
            Toast.makeText(Activity_Chargement.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return listeCategories;
    }




  ////////////////Retourner liste des produits /////////////////////////////
    private List<Produit> getListProduit(int i) {
        mService.getListProduit(i).enqueue(new Callback<List<Produit>>() {
            @Override
            public void onResponse(Call<List<Produit>> call, Response<List<Produit>> response) {

                listeProduits = response.body();
                ArrayAdapter<Produit> dataAdapterProduits = new ArrayAdapter<Produit>(Activity_Chargement.this, android.R.layout.simple_spinner_item, listeProduits);
                dataAdapterProduits.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerNomP.setAdapter(dataAdapterProduits);

            }

            @Override
            public void onFailure(Call<List<Produit>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        return listeProduits;
    }



    public void InsertDataInList() {

       boolean test = false;

        qteProduit = qte.getText().toString().trim();


       for (int i=0; i<produitList.size(); i++) {

            if (produitList.get(i).get(KEY_CODE_PRODUIT).equals(currentProduit.code_produit))
            {
                testExist =i;
                test= true;
            }

        }

            if (qteProduit.length() > 0) {
                qteP = Integer.valueOf(qteProduit);

                if (qteP > 0) {

                    try {

                        final HashMap<String, String> map = new HashMap<String, String>();

                        if (!test) {

                            map.put(KEY_CATEGORIE, currentCategorie.designation);
                            map.put(KEY_PRODUIT, currentProduit.nom_produit);
                            map.put(KEY_QUANTITE, qteP + "");
                            map.put(KEY_CODE_PRODUIT, currentProduit.code_produit);

                            produitList.add(map);


                            list.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            spinnerDepot.setEnabled(false);


                            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                                @Override
                                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                               int pos, long arg3) {
                                    deleteItem = pos;
                                    // Creating a new alert dialog to confirm the delete
                                    AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                                            .setTitle("Etes vous sûr de vouloir supprimer ce produit")
                                            .setPositiveButton("Oui",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int whichButton) {

                                                            try {
                                                                produitList.remove(deleteItem);
                                                                adapter.notifyDataSetChanged();
                                                                dialog.dismiss();
                                                            } catch (Exception e) {
                                                                Toast.makeText(Activity_Chargement.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                            }

                                                        }
                                                    })
                                            .setNegativeButton("Non",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int whichButton) {
                                                            // When you press cancel, just close the
                                                            // dialog
                                                            dialog.cancel();
                                                        }
                                                    }).show();


                                    return false;

                                }
                            });

                       }

                        else
                        {

                            new AlertDialog.Builder(Activity_Chargement.this)
                                    .setTitle("Valider la quantité du produit ")
                                    .setMessage("Etes vous sûr de vouloir mettre à jour la quantité de ce produit ?")
                                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                        @TargetApi(Build.VERSION_CODES.N)
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            int q = Integer.parseInt(produitList.get(testExist).get(KEY_QUANTITE));
                                            int sommeQte= q + qteP;

                                                produitList.get(testExist).put(KEY_QUANTITE, sommeQte+"");

                                                list.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();

                                       }
                                    }).setNegativeButton("Non", null).show();
                        }

                    } catch (Exception ex) {
                        Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                   // Toast.makeText(this, "Veuillez insérer une valeur positive", Toast.LENGTH_SHORT).show();

                    final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Chargement.this).create();
                    alertDialog.setTitle("Erreur");
                    alertDialog.setMessage("Veuillez insérer une valeur positive");
                    alertDialog.setIcon(R.drawable.error);
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.cancel();
                        } });
                    alertDialog.show();

                }
            } else {

              //  Toast.makeText(this, "Veuillez insérer la quantité", Toast.LENGTH_SHORT).show();

                final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Chargement.this).create();
                alertDialog.setTitle("Erreur");
                alertDialog.setMessage("Veuillez insérer la quantité");
                alertDialog.setIcon(R.drawable.error);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.cancel();
                    } });
                alertDialog.show();


            }

    }



    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {

    }


    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }



    public void Clear_Quantite()
    {
        qte.setText("");
    }




    /////////////////fonction pour ajouter le bon de chargement ////////////////
    public void Add_bon_chargement()
    {

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .createChargement(num_chargement, idDepot);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        String s= null;

                        try {
                            s = response.body().string();
                            String h = s.replace("\"", "");

                            if (h.equals("Echec d insertion")) {
                               // Toast.makeText(Activity_Chargement.this, h, Toast.LENGTH_LONG).show();

                                final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Chargement.this).create();
                                alertDialog.setTitle("Erreur");
                                alertDialog.setMessage("Echec d'insertion");
                                alertDialog.setIcon(R.drawable.error);
                                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        alertDialog.cancel();
                                    } });
                                alertDialog.show();


                            } else {

                                currentChargement.id_chargement = Integer.parseInt(h);

                                ///////Appel à la la méthode AddDetailsChargement pour ajouter les détails de bon de chargement dont id_chargement = id_chargement

                                Add_details_chargement(currentChargement.id_chargement);
                                Calendar c = Calendar.getInstance();
                                int year = c.get(Calendar.YEAR);


                               nombre_chargement = nombre_chargement + 1;
                               num_chargement = "Chargement " + year + "_" + nombre_chargement;

                               produitList.clear();
                               adapter.notifyDataSetChanged();

                            }
                        }
                        catch(Exception ex)
                            {
                                Toast.makeText(Activity_Chargement.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

    }



    //////////////////fonction pour ajouter détails chargements ////////////////
    public void Add_details_chargement(int id_chargement)
    {

        for (int i=0; i<produitList.size(); i++) {

               currentDetailsChargement.code_produit = produitList.get(i).get(KEY_CODE_PRODUIT);
               currentDetailsChargement.quantite = Integer.parseInt(produitList.get(i).get(KEY_QUANTITE));

               Call<ResponseBody> call2 = RetrofitClient
                       .getInstance()
                       .getApi()
                       .createDetailsChargement(idDepot, currentDetailsChargement.code_produit, currentDetailsChargement.quantite, id_chargement);

               call2.enqueue(new Callback<ResponseBody>() {
                   @Override
                   public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                       String s = null;

                       try {

                           s = response.body().string();
                          // String h = s.replace("\"", "");
                          // Toast.makeText(Activity_Chargement.this,  s, Toast.LENGTH_SHORT).show();


                       } catch (IOException e) {
                           e.printStackTrace();
                       }

                   }


                   @Override
                   public void onFailure(Call<ResponseBody> call, Throwable t) {

                       Toast.makeText(Activity_Chargement.this, "erreur", Toast.LENGTH_SHORT).show();
                   }

               });


           }


        final AlertDialog alertDialog = new AlertDialog.Builder(Activity_Chargement.this).create();
        alertDialog.setTitle("Succès");
        alertDialog.setMessage("Opération a été effectuée avec succès");
        alertDialog.setIcon(R.drawable.succes);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            } });
        alertDialog.show();



    }







}
