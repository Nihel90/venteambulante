package com.newprojectsql.ambulante.activity.Chargement;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Vente.Produit;
import com.newprojectsql.ambulante.activity.Vente.Vente;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterListProd_Chargement extends BaseAdapter {

    private List<Produit> listProduit;

    private Activity activity;
    private ArrayList< HashMap<String, String> > data;
    private static LayoutInflater inflater=null;

    public AdapterListProd_Chargement(List<Produit> listProduit) {
        this.listProduit = listProduit;
    }



    public AdapterListProd_Chargement(Activity a, ArrayList< HashMap<String, String> > d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_prod_chargement, null);

        TextView categorie = (TextView)vi.findViewById(R.id.categorie); //
        TextView nom_produit = (TextView)vi.findViewById(R.id.produit); //
        TextView qte = (TextView)vi.findViewById(R.id.qte); //
        TextView code = (TextView)vi.findViewById(R.id.codeP);


        HashMap<String, String> produit = new HashMap<String, String>();
        produit = data.get(position);

        // Setting all values in listview
        categorie.setText(produit.get(Activity_Chargement.KEY_CATEGORIE));
        nom_produit.setText(produit.get(Activity_Chargement.KEY_PRODUIT));
        qte.setText(produit.get(Activity_Chargement.KEY_QUANTITE));
        code.setText(produit.get(Activity_Chargement.KEY_CODE_PRODUIT));



        return vi;
    }


}
