package com.newprojectsql.ambulante.activity.GestionClients;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Chercher_client extends AppCompatActivity{

    Api mService;
    private Button  btnAdd, btn_retour_menu;

    private RecyclerView lst_clt;

    @Override
   public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chercher_client);


        mService=RetrofitClient.getInstance().getApi();


      lst_clt= (RecyclerView)findViewById(R.id.lst_clt);
      lst_clt.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
      lst_clt.setHasFixedSize(true);


       getListeClt();



       btnAdd = (Button) findViewById(R.id.btn_addclient);
       btn_retour_menu = (Button) findViewById(R.id.btn_retour_menu);



       btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity = new Intent(Chercher_client.this, Ajouter_client.class);
                startActivity(activity);
            }
        });



       btn_retour_menu.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent activity = new Intent(Chercher_client.this, MainActivity.class);
               startActivity(activity);
           }
       });

    }



    private void getListeClt() {
    mService.getListeClt().enqueue(new Callback<List<Client>>() {
        @Override

        public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {

            displayList(response.body());

        }

        @Override
        public void onFailure(Call<List<Client>> call, Throwable t) {
            Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
        }
    });

    }



    private void displayList(List<Client> clients){

       ClientAdapter adapter = new ClientAdapter(Chercher_client.this, clients);
       lst_clt.setAdapter(adapter);

    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }




}
