package com.newprojectsql.ambulante.activity.GestionClients;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.AuthentifUser.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ClientAdapter extends RecyclerView.Adapter<ClientViewHolder> {

   Context context;
   public  final static String SER_KEY = "com.easyinfogeek.objectPass.ser";
   List<Client> clients;


   public static Client currentClient=null;



    public ClientAdapter(Context context, List<Client> clients) {
        this.context = context;
        this.clients = clients;
    }

    @NonNull
    @Override
    public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.menu_item_layout, null);
        return new ClientViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientViewHolder holder, final int position) {


        //Load Image
        holder.txt_raison.setText(clients.get(position).raison_sociale);
        holder.txt_telMobile.setText(clients.get(position).tel_mobile+"");


        holder.setItemClickListener(new ItemClickListener() {


            @Override
          public void onClick(View v) {

              currentClient = clients.get(position);


            //To pass:
                // in Activity A, when starting Activity B

                Intent intent = new Intent(context, Fiche_client.class);

                Client client= new Client (currentClient.id_client, currentClient.code_client, currentClient.raison_sociale,
                        currentClient.matricule_fiscale, currentClient.registre_commerce, currentClient.email,
                        currentClient.tel_mobile, currentClient.tel_fixe, currentClient.latitude, currentClient.longitude);


                intent.putExtra("CLIENT", client);
                context.startActivity(intent);



                    }
      });

    }

    @Override
    public int getItemCount() {
        return clients.size();
    }
}
