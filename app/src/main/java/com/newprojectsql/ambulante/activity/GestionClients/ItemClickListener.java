package com.newprojectsql.ambulante.activity.GestionClients;

import android.view.View;

public interface ItemClickListener {
    void onClick(View v);
}
