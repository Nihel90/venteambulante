package com.newprojectsql.ambulante.activity.Vente;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.GestionClients.Adresse;
import com.newprojectsql.ambulante.activity.GestionClients.Api;
import com.newprojectsql.ambulante.activity.GestionClients.Client;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Vente extends Activity implements AdapterView.OnItemSelectedListener {

    /////Declaration Api
    Api mService;
    Context context;

    ///// Declaration des Views
    private List<Client> listeClients;
    private List<Categorie> listeCategories;
    public  List<Produit> listeProduits;
    private List<Adresse> listeAdresses;
    private List<HashMap<String, String>> listeLigneFacture;


    private Spinner spinnerClients, spinnerCategorie, spinnerNomP;

    private Button btn_valider, btn_suivant;
    private TextView txt_prix_unitaire, txt_prix_total, txt_solde_client;
    private  EditText qte;

    public String item;
    public  Categorie currentCategorie = null;
    public Produit currentProduit = null;

    public LigneFacture currentLigneFacture;
    public Facture currentFacture;

    public  Client currentClient=null;



    public Client c;
    public static Adresse currentAddress = null;


    int idCategorie, idProduit, id_client;
    String raison_sociale;
    Double prixP = 0.0, prixTot_Produit = 0.0;
    Double prix_ht = 0.0;
    Double prix_ttc=0.0, prix_ttc_total=0.0;

    public double soldeClient= 0.0;
    public double max_credit=0.0;


    int tva;
    int qteP;
    public String qteProduit;

    int id_depot =1;
    int qte_stock;
    int sumQte;

    public   int testExist=-1;


    // contains the id of the item we are about to delete
    public int deleteItem;


    //////////////////////////////////////////////
///////////////////////////////////////////////////////
    static final String KEY_NOM_PRODUIT = "nomP";
    static final String KEY_CODE_PRODUIT = "codeP";
    static final String KEY_PRIX_U = "prixU";
    static final String KEY_QUANTITE = "quantite";
    static final String KEY_PRIX_TOTAL = "prixTotal";
    static final String KEY_PRIX_TTC = "prix_ttc";
    static final String KEY_PRIX_TTC_TOTAL = "prix_ttc_total";
    static final String KEY_TVA = "tva";




    private ListView list;

    private Produit produit;

    private TextView nom_produit;
    private TextView prix_unitaire;
    private TextView quantite;
    private TextView prix_total;
    private TextView txt_prix_ttc;
    private TextView txt_prix_ttc_total;
    private TextView txt_tva;

    AdapterListProduit adapter;
    ArrayList<HashMap<String, String>> produitList;

    ////////////////////////////////////////////////////////////////////
    /////////Déclaration des données relatives au facture //////////////

    String num_facture;
    private int nombre_facture = 0;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vente);


        /////////////////////////////Intent Client///////////////
        currentClient = (Client) getIntent().getSerializableExtra("CLIENT");


        /////////////////////////////////////////////////////
        currentLigneFacture = new LigneFacture();
        currentFacture = new Facture();

        listeCategories = new ArrayList<>();
        listeProduits = new ArrayList<>();
        listeClients = new ArrayList<>();




        ////////////////////////get nbombre des factures ////////////////////////
    Call<Integer> call = RetrofitClient
                .getInstance()
                .getApi()
                .getNbFacture();

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {

                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);

                int s;

                s = response.body();
                //  String h = s.replace("\"", "");

                nombre_facture = s;
                nombre_facture = nombre_facture + 1;
                num_facture = "Fac_" + year + "_" + nombre_facture;

            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

                Toast.makeText(Vente.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }

        });




        //////////////////////////////////////////////////////
        produitList = new ArrayList<HashMap<String, String>>();
        listeLigneFacture = new ArrayList<HashMap<String, String>>();

        adapter = new AdapterListProduit(Vente.this, produitList);

        mService = RetrofitClient.getInstance().getApi();

        // Spinner element
        spinnerClients = (Spinner) findViewById(R.id.spinnerClient);
        spinnerCategorie = (Spinner) findViewById(R.id.spinnerCategorie);
        spinnerNomP = (Spinner) findViewById(R.id.spinnerNomP);


        txt_prix_unitaire = (TextView) findViewById(R.id.txtPrix);
        txt_prix_total = (TextView) findViewById(R.id.txtPrix_total);
        txt_solde_client = (TextView) findViewById(R.id.txt_solde_client);

        qte = (EditText) findViewById(R.id.quantite);
        btn_valider = (Button) findViewById(R.id.btn_valider);
        btn_suivant = (Button) findViewById(R.id.btn_suivant);


        listeCategories = getListCategories();



        //apres spinner
        if( currentClient==null)
        {

            listeClients = getListeClt();

            // Spinner click listener

            spinnerClients.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    // On selecting a spinner item

                    currentClient = listeClients.get(position);
                    id_client = currentClient.id_client;
                    raison_sociale = currentClient.raison_sociale;



                    Call<Double> call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .getTotalCredit(id_client);

                    call.enqueue(new Callback<Double>() {
                        @Override
                        public void onResponse(Call<Double> call, Response<Double> response) {


                            Double s;

                            s=response.body();
                            txt_solde_client.setText(s.toString());

                            soldeClient = s;



                        }

                        @Override
                        public void onFailure(Call<Double> call, Throwable t) {


                        }
                    });


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });



        }
        else {

            try {


                String[] items = new String[] {currentClient.raison_sociale};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerClients.setAdapter(adapter);

                spinnerClients.setEnabled(false);

                id_client = currentClient.id_client;
                raison_sociale = currentClient.raison_sociale;




                Call<Double> call2 = RetrofitClient
                        .getInstance()
                        .getApi()
                        .getTotalCredit(id_client);

                call2.enqueue(new Callback<Double>() {
                    @Override
                    public void onResponse(Call<Double> call, Response<Double> response) {


                        Double s;

                        s=response.body();
                        txt_solde_client.setText(s.toString());

                        soldeClient= s;

                    }

                    @Override
                    public void onFailure(Call<Double> call, Throwable t) {


                    }
                });

            }

            catch (Exception ex)
            {
                Toast.makeText(this, ex.getMessage() , Toast.LENGTH_SHORT).show();}


        }





        spinnerCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // On selecting a spinner item
                currentCategorie = listeCategories.get(position);
                idCategorie = currentCategorie.id_categorie;
                listeProduits = getListProduit(idCategorie);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        spinnerNomP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                // On selecting a spinner item
                currentProduit = listeProduits.get(position);
                idProduit = currentProduit.id_produit;
                prixP = currentProduit.prix;
                prix_ttc = currentProduit.prix_ttc;

                prix_ttc_total = prix_ttc * qteP;
                tva = currentProduit.tva;
                txt_prix_unitaire.setText(prix_ttc.toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        nom_produit = (TextView) findViewById(R.id.nom_produit);
        prix_unitaire = (TextView) findViewById(R.id.prixU);
        quantite = (TextView) findViewById(R.id.qte);
        prix_total = (TextView) findViewById(R.id.prixTotal);
        txt_prix_ttc = (TextView)findViewById(R.id.prix_ttc);
        txt_prix_ttc_total = (TextView) findViewById(R.id.prix_ttc_total);
        txt_tva = (TextView) findViewById(R.id.tva);


        produit = new Produit();
        list = (ListView) findViewById(R.id.list);

        UpdateTotal();




        btn_valider.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                InsertDataInList();
                Clear_Quantite();

            }
        });




        btn_suivant.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                //////////////////get max_credit/////////////////

                Call<Double> call1 = RetrofitClient
                        .getInstance()
                        .getApi()
                        .getMax_Credit(id_client);

                call1.enqueue(new Callback<Double>() {
                    @Override
                    public void onResponse(Call<Double> call, Response<Double> response) {

                        double s= response.body();

                        max_credit=s;

                        double calcul = prix_ttc_total + soldeClient;

                        if (produitList.size() != 0) {

                            if (calcul < max_credit) {

                                new AlertDialog.Builder(Vente.this)
                                        .setTitle("Valider la vente des produits ")
                                        .setMessage("Etes vous sûr de vouloir valider la vente de ces produits ?")
                                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                getDetailsFacture();

                                            }
                                        })

                                        .setNegativeButton("Non", null).show();

                            } else {

                                Toast.makeText(Vente.this, "Vous avez dépassé le montant de crédit autorisé qui est: " + max_credit, Toast.LENGTH_SHORT).show();
                            }
                        }

                        else
                        {
                            Toast.makeText(Vente.this, "Veuillez insérer les produits", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Double> call, Throwable t) {

                    }
                });

            }

        });

    }



    public void InsertDataInList() {

        qteProduit = qte.getText().toString().trim();



        ////////////////////////get qte stock d'un produit ////////////////////////
        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .getQteStock(currentProduit.code_produit, id_depot);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String s = null;
                try {
                    s = response.body().string();

                    String h = s.replace("\"", "");




                    //////////////déclaration es variables pour tester l'existance du produit

                    boolean test = false;


                    for (int i=0; i<produitList.size(); i++) {

                        if (produitList.get(i).get(KEY_CODE_PRODUIT).equals(currentProduit.code_produit))
                        {
                            testExist =i;
                            test= true;                        }

                    }



                    if (qteProduit.length() > 0) {
                        qteP = Integer.valueOf(qteProduit);

                        if (qteP > 0) {

                            if (h.equals("produit non disponible en stock")) {

                                Toast.makeText(Vente.this, s, Toast.LENGTH_LONG).show();

                            } else

                               {
                                qte_stock = Integer.parseInt(h);

                                if (qteP <= qte_stock) {

                                    prixTot_Produit = prixP * qteP;
                                    prix_ttc_total = prix_ttc * qteP;
                                    prixTot_Produit = Math.round(prixTot_Produit * 1000.0) / 1000.0;
                                    prix_ttc_total = Math.round(prix_ttc_total * 1000.0) / 1000.0;


                                    try {

                                        if (!test) {

                                            HashMap<String, String> map = new HashMap<String, String>();

                                            map.put(KEY_NOM_PRODUIT, currentProduit.nom_produit);
                                            map.put(KEY_CODE_PRODUIT, currentProduit.code_produit);
                                            map.put(KEY_PRIX_U, prixP + "");
                                            map.put(KEY_QUANTITE, qteP + "");
                                            map.put(KEY_PRIX_TOTAL, prixTot_Produit + "");
                                            map.put(KEY_PRIX_TTC, prix_ttc + "");
                                            map.put(KEY_PRIX_TTC_TOTAL, prix_ttc_total + "");
                                            map.put(KEY_TVA, tva + "");


                                            produitList.add(map);

                                            list.setAdapter(adapter);

                                            adapter.notifyDataSetChanged();

                                            spinnerClients.setEnabled(false);

                                            registerForContextMenu(list);


                                      /*      list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                                                @Override
                                                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                                               int pos, long arg3) {
                                                    deleteItem = pos;

                                                    // Creating a new alert dialog to confirm the delete
                                                    AlertDialog alert = new AlertDialog.Builder(arg1.getContext())
                                                            .setTitle("Etes vous sûr de vouloir supprimer ce produit")
                                                            .setPositiveButton("Oui",
                                                                    new DialogInterface.OnClickListener() {
                                                                        public void onClick(DialogInterface dialog,
                                                                                            int whichButton) {

                                                                            try {
                                                                                produitList.remove(deleteItem);
                                                                                adapter.notifyDataSetChanged();
                                                                                dialog.dismiss();
                                                                                UpdateTotal();

                                                                            } catch (Exception e) {
                                                                                Toast.makeText(Vente.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                                            }

                                                                        }
                                                                    })
                                                            .setNegativeButton("Non",
                                                                    new DialogInterface.OnClickListener() {
                                                                        public void onClick(DialogInterface dialog,
                                                                                            int whichButton) {
                                                                            // When you press cancel, just close the
                                                                            // dialog
                                                                            dialog.cancel();
                                                                        }
                                                                    }).show();


                                                    return false;

                                                }
                                            });  */

                                            UpdateTotal();
                                        }

                                        else
                                        {

                                     new AlertDialog.Builder(Vente.this)
                                    .setTitle("Valider la quantité du produit ")
                                    .setMessage("Etes vous sûr de vouloir mettre à jour la quantité de ce produit ?")
                                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            int q = Integer.parseInt(produitList.get(testExist).get(KEY_QUANTITE));
                                            int sommeQte= q + qteP;
                                            Double prix_ttc = Double.valueOf(produitList.get(testExist).get(KEY_PRIX_TTC));
                                            Double prix_ttc_total = sommeQte*prix_ttc;
                                            prix_ttc_total = Math.round(prix_ttc_total*1000.0)/1000.0;


                                            if (sommeQte<= qte_stock) {
                                                produitList.get(testExist).put(KEY_QUANTITE, sommeQte + "");
                                                produitList.get(testExist).put(KEY_PRIX_TTC_TOTAL, prix_ttc_total +"");
                                                UpdateTotal();

                                                list.setAdapter(adapter);
                                                adapter.notifyDataSetChanged();
                                            }

                                            else
                                            {
                                                Toast.makeText(Vente.this, "La quantité disponible pour ce produit est " + qte_stock, Toast.LENGTH_SHORT).show();
                                            }

                                            }})


                                    .setNegativeButton("Non", null).show();

                                        }


                                    } catch (Exception ex) {
                                        Toast.makeText(Vente.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(Vente.this, "La quantité disponible est " + qte_stock, Toast.LENGTH_SHORT).show();
                                }
                            }

                            } else{
                                Toast.makeText(Vente.this, "Veuillez insérer une valeur positive", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Vente.this, "Veuillez insérer la quantité", Toast.LENGTH_SHORT).show();
                        }




                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.setHeaderTitle("Choisir une action");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){

        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();

        if(item.getItemId()==R.id.supprimer){


            try {
                produitList.remove(info.position);
                adapter.notifyDataSetChanged();

                Toast.makeText(getApplicationContext(),"Produit supprimé avec succès",Toast.LENGTH_LONG).show();

                UpdateTotal();

            } catch (Exception e) {
                Toast.makeText(Vente.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }


        }
        else if(item.getItemId()==R.id.changer_qte){


            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Changer la quantité ");
            alert.setMessage("Nouvelle quantité");

            // Set an EditText view to get user input
            final EditText input = new EditText(Vente.this);
            alert.setView(input);

            //need to add two more edit text fields for extra input.


            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Editable value = input.getText();

                    String s = value.toString();

                    double var = Double.parseDouble(s);

                    // Do something with value!

                    Double prix_ttc = Double.valueOf(produitList.get(info.position).get(KEY_PRIX_TTC));
                    Double prix_ttc_total = var * prix_ttc;
                    prix_ttc_total = Math.round(prix_ttc_total*1000.0)/1000.0;

                    if(var <= qte_stock) {
                        produitList.get(info.position).put(KEY_QUANTITE, value + "");
                        produitList.get(info.position).put(KEY_PRIX_TTC_TOTAL, prix_ttc_total + "");

                        UpdateTotal();
                        list.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        Toast.makeText(getApplicationContext(), "quantité modifiée avec succès", Toast.LENGTH_LONG).show();
                    }

                    else
                    {
                        Toast.makeText(Vente.this, "la quantité disponible en stock est "+qte_stock, Toast.LENGTH_SHORT).show();
                    }

                }
            });

            alert.setNegativeButton("Retour", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.

                }
            });

            alert.show();
            return true;


        }else{
            return false;



        }
        return true;
    }


    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {

    }


    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }


    private List<Client> getListeClt() {

        mService.getListeClt().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {


                listeClients = response.body();
                ArrayAdapter<Client> dataAdapterClients = new ArrayAdapter<Client>(Vente.this, android.R.layout.simple_spinner_item, listeClients);
                dataAdapterClients.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerClients.setAdapter(dataAdapterClients);


            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {

            }
        });

      return listeClients;
    }





    private List<Categorie> getListCategories() {
        try {
            mService.getListCategories().enqueue(new Callback<List<Categorie>>() {
                @Override
                public void onResponse(Call<List<Categorie>> call, Response<List<Categorie>> response) {

                    listeCategories = response.body();
                    ArrayAdapter<Categorie> dataAdapterCategories = new ArrayAdapter<Categorie>(Vente.this, android.R.layout.simple_spinner_item, listeCategories);
                    dataAdapterCategories.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerCategorie.setAdapter(dataAdapterCategories);

                }

                @Override
                public void onFailure(Call<List<Categorie>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return listeCategories;
    }


    private List<Produit> getListProduit(int i) {
        mService.getListProduit(i).enqueue(new Callback<List<Produit>>() {
            @Override
            public void onResponse(Call<List<Produit>> call, Response<List<Produit>> response) {

                listeProduits = response.body();
                ArrayAdapter<Produit> dataAdapterProduits = new ArrayAdapter<Produit>(Vente.this, android.R.layout.simple_spinner_item, listeProduits);
                dataAdapterProduits.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerNomP.setAdapter(dataAdapterProduits);

            }

            @Override
            public void onFailure(Call<List<Produit>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });


        return listeProduits;
    }




    ///////////////////update prix_ht /////////////////
    //////////////////////////////////////////////////

    public void UpdateTotal()
    {
        Double s=0.0;
        Double s1=0.0;

        for (int i=0; i<produitList.size(); i++)
        {

            prix_ttc_total=Double.valueOf(produitList.get(i).get(KEY_PRIX_TTC_TOTAL));
            prix_ht = Double.valueOf(produitList.get(i).get(KEY_PRIX_TOTAL));


            s=s+prix_ttc_total;
            s= Math.round(s*1000.0)/1000.0;

            s1=s1+prix_ht;
            s1= Math.round(s1*1000.0)/1000.0;

        }

        txt_prix_total.setText(s.toString());
        prix_ttc_total = s;
        prix_ht=s1;

    }


    public void Clear_Quantite()
    {
        qte.setText("");
    }



    /////////////////////get details facture /////////// intent///////////////

    public void getDetailsFacture()
    {
        Intent intent1 = new Intent(Vente.this, Valider_Vente.class);

        Facture facture = new Facture (num_facture, id_client, raison_sociale, prix_ht, prix_ttc_total);

              Client client = new Client(currentClient.code_client, currentClient.raison_sociale, currentClient.matricule_fiscale,
                      currentClient.registre_commerce, max_credit);


        ArrayList<HashMap<String, String>> produitList2 = new ArrayList<>();
        for (int i=0; i<produitList.size(); i++)
        {
            produitList2.add(produitList.get(i));

        }

        intent1.putExtra("FACTURE", facture);
        intent1.putExtra("LIGNESFACTURE", produitList2);
        intent1.putExtra("CLIENT", client);


        Vente.this.startActivity(intent1);

    }











}






