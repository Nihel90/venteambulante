package com.newprojectsql.ambulante.activity.Chargement;

import java.util.Date;

public class BonChargement {

    public int id_chargement;
    public Date date_chargement;
    public String num_chargement;
    public int id_depot;


    public BonChargement() {
    }

    public BonChargement(int id_chargement, Date date_chargement, String num_chargement, int id_depot) {
        this.id_chargement = id_chargement;
        this.date_chargement = date_chargement;
        this.num_chargement = num_chargement;
        this.id_depot = id_depot;
    }


    public int getId_chargement() {
        return id_chargement;
    }

    public Date getDate_chargement() {
        return date_chargement;
    }

    public String getNum_chargement() {
        return num_chargement;
    }

    public int getId_depot() {
        return id_depot;
    }


    public void setId_chargement(int id_chargement) {
        this.id_chargement = id_chargement;
    }


    public void setDate_chargement(Date date_chargement) {
        this.date_chargement = date_chargement;
    }


    public void setNum_chargement(String num_chargement) {
        this.num_chargement = num_chargement;
    }


    public void setId_depot(int id_depot) {
        this.id_depot = id_depot;
    }



}
