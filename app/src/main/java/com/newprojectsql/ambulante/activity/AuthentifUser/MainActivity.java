package com.newprojectsql.ambulante.activity.AuthentifUser;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.newprojectsql.ambulante.R;
import com.newprojectsql.ambulante.activity.Chargement.Activity_Chargement;
import com.newprojectsql.ambulante.activity.GestionClients.Chercher_client;
import com.newprojectsql.ambulante.activity.GestionClients.RetrofitClient;
import com.newprojectsql.ambulante.activity.Inventaire.Activity_Inventaire;
import com.newprojectsql.ambulante.activity.Parametres.ParametreActivity;
import com.newprojectsql.ambulante.activity.Reglement.Activity_RegPrincipale;
import com.newprojectsql.ambulante.activity.Vente.Vente;
import com.newprojectsql.ambulante.helper.SQLiteHandler;
import com.newprojectsql.ambulante.helper.SessionManager;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.SHARED_PREFS;
import static com.newprojectsql.ambulante.activity.Parametres.ParametreActivity.TEXT;


////////////////@ Guedri Nihel ////////////////


public class MainActivity extends Activity {




    private CardView bChargement, bClients, bVente, bReglement, bInventaire, bParam;
    private Button btnLogout;

    private SQLiteHandler db;
    private SessionManager session;

    private int idDepot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        bChargement= (CardView)findViewById(R.id.btn_chargement);
        bClients= (CardView)findViewById(R.id.btn_clients);
        bVente= (CardView)findViewById(R.id.btn_vente);
        bReglement= (CardView)findViewById(R.id.btn_reglement);
        bInventaire= (CardView)findViewById(R.id.btn_inventaire);
        bParam= (CardView)findViewById(R.id.btn_param);
        btnLogout = (Button) findViewById(R.id.btnLogout);


        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }



        bChargement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent activityChargement = new Intent(MainActivity.this, Activity_Chargement.class);
                    startActivity(activityChargement);
                    ;}

                catch (Exception Ex)
                {
                    Toast.makeText(MainActivity.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });






      bClients.setOnClickListener (new View.OnClickListener() {
        public void onClick(View v) {

          try{
            Intent activityClient = new Intent(MainActivity.this, Chercher_client.class);
            startActivity(activityClient);
            ;}

            catch (Exception Ex)
            {
                Toast.makeText(MainActivity.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
            }


        }
    });


        bVente.setOnClickListener (new View.OnClickListener() {
            public void onClick(View v) {

               try{
                Intent activityVente = new Intent(MainActivity.this, Vente.class);
                startActivity(activityVente);
                ;}
                catch (Exception ex)
                {
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        });


        bReglement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent activityReg = new Intent(MainActivity.this, Activity_RegPrincipale.class);
                    startActivity(activityReg);
                }

                catch (Exception ex)
                {
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });



        bInventaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                String p = sharedPreferences.getString(TEXT, "");

                idDepot = Integer.valueOf(p);


                try {

                    Call<ResponseBody> call =RetrofitClient
                            .getInstance()
                            .getApi()
                            .getCurrentDate();

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                            try {
                                String currentDate = response.body().string();

                                String h = currentDate.replace("\"", "");

                            //    Toast.makeText(MainActivity.this, ""+h, Toast.LENGTH_SHORT).show();

                                Call<Boolean> call1 = RetrofitClient
                                        .getInstance()
                                        .getApi()
                                        .isInventaireExist(idDepot, h);


                                call1.enqueue(new Callback<Boolean>() {
                                    @Override
                                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                                        ////////get response : vérifier si inventaire existe déjà pour la date courante///////
                                        Boolean s = response.body();

                                        if (s) {
                                            new AlertDialog.Builder(MainActivity.this)
                                                    .setTitle("Un inventaire existe déjà ")
                                                    .setMessage("Souhaitez-vous mettre à jour cet inventaire?")
                                                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {

                                                            Intent activityInv = new Intent(MainActivity.this, Activity_Inventaire.class);
                                                            startActivity(activityInv);

                                                        }
                                                    }).setNegativeButton("Non", null).show();
                                        } else {
                                            Intent activityInv = new Intent(MainActivity.this, Activity_Inventaire.class);
                                            startActivity(activityInv);

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Boolean> call, Throwable t) {

                                    }
                                });

                            }

                           catch (IOException e) {
                            e.printStackTrace();
                           }


                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });

                }

                catch (Exception ex)
                {
                    Toast.makeText(MainActivity.this, ""+ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });





        bParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    Call<ResponseBody> call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .getPasswordParametre();

                    call.enqueue(new Callback<ResponseBody>() {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            try {
                                String s = response.body().string();

                                final String h = s.replace("\"", "");


                                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                                alert.setTitle("Saisir le mot de passe ");
                                alert.setMessage("Mot de passe d'utilisateur");

                                // Set an EditText view to get user input
                                final EditText input = new EditText(MainActivity.this);
                                alert.setView(input);

                                //need to add two more edit text fields for extra input.

                                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Editable value = input.getText();

                                        String s = value.toString();

                                        if(s.equals(h)) {

                                            Intent activityParametre = new Intent(MainActivity.this, ParametreActivity.class);
                                            startActivity(activityParametre);
                                        }

                                        else
                                        {

                                            final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                            alertDialog.setTitle("Erreur");
                                            alertDialog.setMessage("Votre mot de passe est incorrect");
                                            alertDialog.setIcon(R.drawable.error);
                                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    alertDialog.cancel();
                                                } });
                                            alertDialog.show();

                                        }

                                    }
                                });

                                alert.setNegativeButton("Retour", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                });

                                alert.show();

                            } catch (IOException e) {

                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });


                    ;}

                catch (Exception Ex)
                {
                    Toast.makeText(MainActivity.this, Ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });





        // Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Quitter l'application")
                .setMessage("Etes vous sûr de vouloir quitter l'application ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        session.setLogin(false);
                        db.deleteUsers();

                        // Launching the login activity
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }
                })

                .setNegativeButton("Non", null).show();

    }



    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }


}
