package com.newprojectsql.ambulante.activity.Chargement;

public class Stock {

    public int id_stock;
    public String code_produit;
    public int quantite;
    public int id_depot;


    public Stock() {
    }

    public Stock(int id_stock, String code_produit, int quantite, int id_depot) {
        this.id_stock = id_stock;
        this.code_produit = code_produit;
        this.quantite = quantite;
        this.id_depot = id_depot;
    }

    public int getId_stock() {
        return id_stock;
    }

    public String getCode_produit() {
        return code_produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public int getId_depot() {
        return id_depot;
    }


    public void setId_stock(int id_stock) {
        this.id_stock = id_stock;
    }

    public void setCode_produit(String code_produit) {
        this.code_produit = code_produit;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void setId_depot(int id_depot) {
        this.id_depot = id_depot;
    }


}
